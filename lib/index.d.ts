/**
 * DecodingContext is used for explaining any decode errors
 * null is used to trigger an exception
 */
type Context = DecodingContext | DecodingConfig | null;
type TupleUnion<T extends ReadonlyArray<unknown>> = T[number];
type ObjectPropertiesUnion<O extends object> = O[keyof O];
type ArrayMember<T> = T extends ReadonlyArray<infer U> ? U : never;
/**
 * The Type of a function that decodes (generally unknown) IN data into a known type OUT
 */
export type DecoderFn<OUT, IN = any> = (unknown: IN, ...args: any) => OUT;
type UnDecoderFn<DECODER> = DECODER extends Decoder ? UnDecoder<DECODER> : never;
type ObjectMapUnDecoderFn<DECODERS> = {
    [K in keyof DECODERS]: UnDecoderFn<DECODERS[K]>;
};
export type Decoder<SOURCE = any> = Array<Decoder> | {
    [key: string]: Decoder;
} | DecoderFn<any, SOURCE> | number | string | boolean;
type UnDecoder<DT extends Decoder> = DT extends DecoderFn<infer T> ? T : DT extends Array<Decoder> ? Array<UnionMapUnDecoder<TupleUnion<DT>>> : DT extends {
    [key: string]: Decoder;
} ? {
    [K in keyof DT]: UnDecoder<DT[K]>;
} : DT extends string | number | boolean | null ? DT : never;
type UnionMapUnDecoder<DT extends Decoder> = DT extends DT ? UnDecoder<DT> : never;
type TupleMapUnDecoder<A extends ReadonlyArray<Decoder<any>>> = {
    [K in keyof A]: (A[K] extends Decoder<any> ? UnDecoder<A[K]> : never);
};
export type ArgDecoder = DecoderFn<any> | {
    [key: string]: ArgDecoder;
} | string | number | boolean;
type UnArgDecoder<DA extends ArgDecoder> = DA extends DecoderFn<infer T> ? T : DA extends {
    [key: string]: ArgDecoder;
} ? ObjectPropertiesUnion<{
    [K in keyof DA]: UnArgDecoder<DA[K]>;
}> : DA extends (string | number | boolean) ? DA : never;
type TupleMapUnArgDecoder<DAs extends ArgDecoder[]> = {
    [K in keyof DAs]: DAs[K] extends ArgDecoder ? UnArgDecoder<DAs[K]> : never;
};
/**
 * An object where the property values are all of the same type.
 */
export type Dict<VALUE> = {
    [_key: string]: VALUE;
};
type UnDict<DICT> = DICT extends Dict<infer VALUE> ? VALUE : never;
/**
 * DecoderOptions that change the way some decoders behave.
 */
export type DecoderOptions = Partial<{
    name: string;
    ignoringErrors: boolean;
    noExtraProps: boolean;
    onlyOne: boolean;
    keyPath: PathSpec;
    valuePath: PathSpec;
    autoCreate: boolean;
    objectLike: boolean;
}>;
export declare const ignoringErrors = true;
export declare const noExtraProps = true;
export declare const onlyOne = true;
export declare const autoCreate = true;
export declare const objectLike = true;
type DecodingConfig = {
    maxErrorCount: number;
};
/**
 * Returns the input json if it is a string
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns string
 * @throws when json isn't a string
 */
export declare const string: DecoderFn<string>;
/**
 * Returns the input json if it is a number
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns number
 * @throws when json isn't a number
 */
export declare const number: DecoderFn<number>;
/**
 * Returns the inout json if it is a boolean
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns boolean
 * @throws when json isn't a boolean
 */
export declare const boolean: DecoderFn<boolean>;
/**
 * Returns the input json if it is a null
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns null
 * @throws when json isn't a null
 */
export declare const literalnull: DecoderFn<null>;
/**
 * Returns the input json if it is a JS array (not recursive)
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an array (any[])
 * @throws when json isn't an array
 */
export declare const literalarray: DecoderFn<Array<any>>;
/**
 * Returns json if it is a JS object (not recursive)
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an object
 * @throws when json isn't an objext
 */
export declare const literalobject: DecoderFn<Dict<any>>;
/**
 * Returns json if it is objectLike (not recursive)
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an object or class
 * @throws when json isn't an objext or class
 */
export declare const literalobjectLike: DecoderFn<Dict<any>>;
/**
 * Build a decoder that returns a default value if there is nothing to decode (eg missing field).
 * By default this combinator will throw if there is a JSON value present, but it fails.
 * If options.ignoringErrors is true, return the default value if the given decoder throws an exception
 *
 * @param decoder to try first
 * @param def the default value to use
 * @options .ignoringErrors
 * @returns the decoded / default value
 * @throws whatever the decoder throws
 */
export declare const def: <DT extends Decoder<any>, DEF extends UnDecoder<DT>>(decodeTree: DT, def: DEF, options?: DecoderOptions) => DecoderFn<UnDecoder<DT>, any>;
/**
 * Build a decoder that returns undefined if there is nothing to decode (ie missing field in object)
 * If options.ignoringErrors is true, return the default value if the given decoder throws an exception
 *
 * @param decoder to use if the field is presnt
 * @returns the decoded value
 * @throws whatever the decoder throws
 */
export declare const opt: <DT extends Decoder<any>>(decoder: DT, options?: DecoderOptions) => DecoderFn<UnDecoder<DT> | undefined, any>;
/**
 * Build a DecoderFn that ignores the provided json value at decode time, and returns the given value
 *
 * @param constantValue to return on decode
 * @returns the constantValue at decode time
 */
export declare const constant: <T>(constantValue: T) => DecoderFn<T, any>;
/**
 * Build a DecoderFn that matches any of the provided values
 *
 * @param values array of values to check the unknown json against
 * @returns the found value
 * @throws if none of the values match
 */
export declare const literalValue: <T extends readonly (string | number | boolean | null)[]>(values: T) => DecoderFn<ArrayMember<T>, any>;
/**
 * Build a DecoderFn that matches one of the provided strings. The return type is a stringLiteral type
 *
 * @param strings to check the unknown json against
 * @returns the string found - a string literal.
 * @throws if none of the strings match
 */
export declare const stringLiteral: <T extends string>(...strings: readonly T[]) => DecoderFn<T, any>;
/**
 * Build a DecoderFn that matches a JSON object where the given fields match the coresponding decoder.
 *
 * Options:
 *
 *   name: decoder name to use in errors
 *
 *   noExtraProps: any extra fields consititute an error
 *   objectLike  : accept classes and objects
 *
 * @param decoders an object of decoders that the unknown json must contain, and the decoders each value must match
 * @param options decoding options (.name & .noExtraProps)
 * @returns an object whos fields consist of the decoder results
 * @throws if any of the field decoders throw, or if there are excess fields in the unkown json
 */
export declare const object: <DPT extends Dict<Decoder<any>>>(decodeTreeDict: DPT, options?: DecoderOptions) => DecoderFn<UnDecoder<DPT>, any>;
/**
 * Build a DecoderFn that matches a hetrogeneous JSON array where each elements matches the
 * coresponding decoder.
 *
 * @param decoders a tuple of decoders that the unknown json must contain.
 * @returns the decoded tuple
 * @throws if any of the element decoders throw
 */
export declare const tuple: <DTA extends any[]>(...decodeTrees: DTA) => DecoderFn<TupleMapUnDecoder<DTA>, any>;
/**
 * Build a decoder that runs all the decoders in a tuple.
 *
 * @param decodeTreeTuple tuple of decoders to try
 * @param options .name for error reporting
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
export declare const tupleT: <DTA extends Decoder<any>[]>(decodeTreeTuple: DTA, options?: DecoderOptions) => DecoderFn<TupleMapUnDecoder<DTA>, any>;
/**
 * Build a dictionary decoder - object where properties are all of the same type, and use the same decoder
 *
 * @param valueDecoder
 * @param options : options.name : name of the decoder to use in errors, safe : ignore values that fail to decode
 * @returns the decoded dictionary
 * @throws if none of the values match
 */
export declare const dict: <DT extends Decoder<any>>(valueDecoder: DT, options?: DecoderOptions) => DecoderFn<Dict<UnDecoder<DT>>, any>;
/**
 * Build a homogeneous array decoder - an array where elements are all of the same type, and use the same decoder
 *
 * @param elementDecoder
 * @param options : options.name : name of the decoder to use in errors, safe : ignore values that fail to decode
 * @returns the decoded array
 * @throws if an element fails to decode and options.safe is not true
 */
export declare const array: <DT extends Decoder<any>>(elementDecodeTree: DT, options?: DecoderOptions) => DecoderFn<UnDecoder<DT>[], any>;
/**
 * Build a decoder for a nested object tree of decoders.
 *
 * @param decodeTree object tree of decoders descibing struture and types
 * @param options Decoding options (name & strict)
 * @returns an object whos fields consist of the decoder results
 * @throws if any of the field decoders throw, or if there are excess fields in the unkown json

 */
export declare const decode: <DT extends Decoder<any>>(decodeTree: DT, options?: DecoderOptions) => DecoderFn<UnDecoder<DT>, any>;
/**
 * Build a decoder that tries a sequence of decoders in turn until one succeeds.
 * (use altOpts if you want to use options)
 *
 * @param decoders tuple of decoders to try
 * @returns the result of the first successful decoder
 * @throws if none of the decoders match
 */
export declare const alt: <DTA extends Decoder<any>[]>(...decodeTreeArray: DTA) => DecoderFn<UnionMapUnDecoder<ArrayMember<DTA>>, any>;
/**
 * Build a decoder (with Options) that tries an array of decoders in turn until one succeeds.
 *
 * @param options .name for error reporting
 * @param decoders tuple of decoders to try
 * @returns the result of the first successful decoder
 * @throws if none of the decoders match
 */
export declare const altT: <DTA extends readonly Decoder<any>[]>(altDecoders: DTA, options?: DecoderOptions) => DecoderFn<UnionMapUnDecoder<ArrayMember<DTA>>, any>;
/**
 * Build a decoder that runs all the decoders in a tuple. They all have to pass or an error is thrown.
 * Each decoder is passed in the top level json
 *
 * @param decodeTreeArray tuple of decoders to try
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
export declare const every: <DTA extends readonly any[]>(...decodeTreeArray: DTA) => DecoderFn<TupleMapUnDecoder<DTA>, any>;
/**
 * Build a decoder that runs all the decoders in a tuple. They all have to pass or an error is thrown.
 * Each decoder is passed in the same json
 *
 * @param decodeTreeArray tuple of decoders to try
 * @param options .name for error reporting
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
export declare const everyT: <DTA extends readonly Decoder<any>[]>(decodeTreeArray: DTA, options?: DecoderOptions) => DecoderFn<TupleMapUnDecoder<DTA>, any>;
/**
 * Build a decoder that runs all the decoders in an object. They all have to pass or an error is thrown.
 * Each decoder is passed in the top level json
 *
 * @param decoders object of decoders to try
 * @param options .name for error reporting
 * @returns an object of the decoder results
 * @throws if any of the decoders throw
 */
export declare const everyO: <DPT extends Dict<Decoder<any>>>(decodeTreeDict: DPT, options?: DecoderOptions) => DecoderFn<UnDecoder<DPT>, any>;
/**
 * Build a decoder that gets a string from a specified field/path in the json, looks up a decoder in the decoders
 * argument and returns the result of trying that decoder.
 *
 * @param lookupPath the field/path of the string to lookup in the json
 * @param decoders the object of decoders to select one from
 * @param options options.name: name of the decoder in any error messages
 * @returns an object of the decoder results
 * @throws if the field/path does not exits, isn't a string, no matching decoder is found, or the decoder throws
 */
export declare const lookup: <DPT extends Dict<Decoder<any>>>(lookupPath: string | ReadonlyArray<string>, decodeTreeDict: DPT, options?: DecoderOptions) => DecoderFn<UnDict<UnDecoder<DPT>>, any>;
type TypeDecoders = {
    boolean: DecoderFn<any, boolean>;
    number: DecoderFn<any, number>;
    string: DecoderFn<any, string>;
    null: DecoderFn<any, null>;
    object: DecoderFn<any, object>;
    array: DecoderFn<any, ReadonlyArray<unknown>>;
    default: DecoderFn<any, unknown>;
};
/**
 * Build a decoder that checks the JSON type of the json, looks up a decoder in the decoders
 * argument based on that type and returns the result of trying that decoder. A 'default'
 * decoder is used if the actual type decoder is missing. Throw if no decoder can be found
 *
 * @param decoders the object of decoders to lookup in
 * @param options options.name: name of the decoder in any error messages
 * @returns the result of the selected decoder
 * @throws if a decoder cannot be found, or the selected decoder throws
 */
export declare function type<P extends Partial<TypeDecoders>>(decoders: P, options?: DecoderOptions): DecoderFn<ObjectPropertiesUnion<ObjectMapUnDecoderFn<P>>>;
export type JsonValue = string | number | boolean | null | JsonArray | JsonObject;
export interface JsonArray extends ReadonlyArray<JsonValue> {
}
export interface JsonObject {
    [member: string]: JsonValue;
}
/**
 * A Decoder that checks recursively that the json is a JSON value.
 * Side-effect is to deep copy the json
 *
 * @param json the value to decoder as a JSON value
 * @param ctx optional Context
 * @throws if the json contains invalid JSON (eg undefined, a Class)
*/
export declare const jsonValue: DecoderFn<JsonValue>;
export declare const UP: null;
export type PathSpec = ReadonlyArray<string | number | null> | number | string;
/**
 * Build a decoder that targets a different part of the unknown JSON and attepts to use the
 * provided decoder.
 *
 * Path is a string using .field or [element] syntax or an array of components (strings /
 * numbers / null). If using an array, a null can be used to travel up the unknown JSON
 * structure.
 *
 * @param path the path to json JSON value
 * @param decoder the decoder to use
 * @param options .name : the name to use in any error messages, .autoCreate : create empty object if necessary when traversing
 * @returns the result of the decoder
 * @throws if the path cannot be travelled or the decoder throws.
 */
export declare const path: <DT extends Decoder<any>>(path: PathSpec, decodeTree: DT, options?: DecoderOptions) => DecoderFn<UnDecoder<DT>, any>;
/**
 * Try to decode something, then pass the result to a user provided function.
 *
 * @param decoder to attempt
 * @param userFn called with the result of the decoder
 * @returns the result of the user function
 * @throws if the decoder fails / user function throws
 */
export declare const withDecoder: <DT extends Decoder<any>, R>(decodeTree: DT, userFn: (t: UnDecoder<DT>, ctx: DecodingContext, json: unknown) => R, options?: DecoderOptions) => DecoderFn<R, any>;
type Validators<T> = Dict<(decoded: T) => boolean>;
/**
 * Try to decode something, pass the result to a user Validators. If all validators returns true, return
 * the results of the decoder, otherwise throw an error.
 *
 * @param decoder to attempt
 * @param validator a dict of function to validate the decoder result.
 * @param options .name: name for the error.
 * @returns the result of the decoder
 * @throws if the decoder fails / validator function fails or throws
 */
export declare const validate: <DT extends Decoder<any>>(decodeTree: DT, validators: Validators<UnDecoder<DT>>, options?: DecoderOptions) => DecoderFn<UnDecoder<DT>, any>;
/**
 * At decode time, try the 'testDecoder', if that succeeds, then use 'passDecoder', otherwise use 'failDecoder' (if provided)
 *
 * @param testDecoder test decoder, any successful results are not used
 * @param passDecoder the decoder to use if the test decoder was successful
 * @param failDecoder the decoder to use if the test decoder fails
 *
 * @returns an object of the decoder results
 * @throws
 */
export declare const ternary: <DT extends Decoder<any>, AT extends Decoder<any>, BT extends Decoder<any>>(testDecodeTree: DT, passDecodeTree: AT, failDecodeTree: BT | undefined, options?: DecoderOptions) => DecoderFn<UnDecoder<AT> | UnDecoder<BT>, any>;
/**
 *
 * @param argSpec
 * @param options
 */
export declare const arg: <AS extends ArgDecoder>(argSpec: AS, options?: DecoderOptions) => DecoderFn<UnArgDecoder<AS>, any>;
export declare const argsT: <ADs extends ArgDecoder[]>(argSpecs: ADs, options?: DecoderOptions) => DecoderFn<TupleMapUnArgDecoder<ADs>, any>;
export declare const args: <DAs extends any[]>(...argSpecs: DAs) => DecoderFn<TupleMapUnArgDecoder<DAs>, any>;
export declare const callT: <ADs extends ArgDecoder[], ARGS extends TupleMapUnArgDecoder<ADs>, R>(userFn: (...t: ARGS) => R, argumnetDecoders: ADs, options?: DecoderOptions) => DecoderFn<R, any>;
export declare const call: <ADs extends ArgDecoder[], ARGS extends TupleMapUnArgDecoder<ADs>, R>(userFn: (...t: ARGS) => R, ...argumnetDecoders: ADs) => DecoderFn<R, any>;
type Constructable<ARGs extends any[], CLASS> = {
    new (...argsT: ARGs): CLASS;
};
export declare const constructor: <ARGs extends any[], CLASS>(clazz: Constructable<ARGs, CLASS>) => (...argsT: ARGs) => CLASS;
export declare const construct: <ADs extends ArgDecoder[], ARGs extends TupleMapUnArgDecoder<ADs>, CLASS>(clazz: Constructable<ARGs, CLASS>, ...argumnetDecoders: ADs) => DecoderFn<CLASS, any>;
/**altT
 * Build a decoder that returns a Set created from a JSON array.
 *
 * @param decoder for each element in the Set: number, string, boolean or null
 * @param options .name & .safe
 * @returns a Set where each member was successfully decoded
 * @throws if a member failed to decode and options.safe is NOT true.
 */
export declare const set: <D extends Decoder<any>>(decoder: D, options?: DecoderOptions) => DecoderFn<Set<UnDecoder<D>>, any>;
/**
 * Build a decoder that returns a Map created from:

 * a) an object,
 * b) an array of tuples
 * c) an array of objects with 2 fields: default 'key' and 'value'.
 *
 * Both the keys and values will be decoded by the respective decoders.
 *
 * @param keyDecoder for each key: string or number
 * @param valueDecoder for each value
 * @param options .name & .safe
 * @returns a Map where each kay & value was successfully decoded
 * @throws if a key or value failed to decode and options.safe is NOT true.
 */
export declare const map: <KT extends Decoder<any>, VT extends Decoder<any>>(keyDecoder: KT, valueDecoder: VT, options?: DecoderOptions) => DecoderFn<Map<UnDecoder<KT>, UnDecoder<VT>>, any>;
/**
 * Decodes ISO 8601 date format into a JS Date Class,
 *
 * @param json unknown data to decode
 * @param ctx optional Context
 * @returns a Date class
 * @throws if a Date cannot be created.
 */
export declare const isodate: DecoderFn<Date, any>;
/**
 * Decodes an number or ISO 8601 date format into a JS Date Class,
 *
 * @param json unknown data to decode
 * @param ctx optional Context
 * @returns a Date class
 * @throws if a Date cannot be created.
 */
export declare const date: DecoderFn<Date>;
/**
 * Returns the input json if it is a number string
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns number
 * @throws when json isn't a number
 */
export declare const numberString: DecoderFn<number>;
/**
 * Utility function to create a decoder from a simple function without needing to worry about context handling.
 * @param userFn
 */
export declare function decoder<T>(userFn: (json: unknown, context: DecodingContext) => T, decoderName?: string): DecoderFn<T>;
type InfoDecoders = undefined | ReadonlyArray<DecoderFn<any, any>> | {
    [key: string]: DecoderFn<any, any> | undefined;
};
export declare class DecodingContext {
    readonly parentContext: DecodingContext | undefined;
    readonly name: string | undefined;
    readonly field: string | number | undefined;
    readonly json: unknown;
    decodingConfig: DecodingConfig;
    constructor(parentContext: DecodingContext | undefined, name: string | undefined, field: string | number | undefined, json: unknown, decodingConfig: DecodingConfig);
    static create(json: unknown, contextOrConfig: Context | DecodingConfig | undefined, name: string | undefined, onNull: InfoDecoders | ((name: string | undefined) => (DecoderInfo | undefined))): DecodingContext;
    push(field: string | number, json: unknown): DecodingContext;
    static expectedList(expected: string | ReadonlyArray<string>): string;
    static throwTypeErrorOrInfo(ctx: Context | undefined, expected: string | ReadonlyArray<string>, json: unknown): never;
    typeError(expected: string | ReadonlyArray<string>, json: unknown): DecodeError;
    decoderError(message: string, json: unknown): DecodeError;
}
declare class DecoderInfo extends Error {
    readonly expected: ReadonlyArray<string>;
    readonly decoders: InfoDecoders;
    constructor(expected: ReadonlyArray<string>, decoders: InfoDecoders);
}
export declare function decoderInfo(decoder: DecoderFn<any>): DecoderInfo | undefined;
export declare class DecodeError extends Error {
    private ctx;
    private errors;
    constructor(errorFn: () => string, ctx: DecodingContext);
    addError(decoderError: DecodeError): DecodeError;
    errorCount(): number;
    get messages(): string[];
    get message(): string;
    private static namePathRootValue;
}
export type Equals<X, Y, T, F> = (<T>() => T extends X ? 1 : 2) extends (<T>() => T extends Y ? 1 : 2) ? T : F;
export declare const checkDecoder: <T, P extends DecoderFn<T, any>>(error: Equals<T, UnDecoderFn<P>, string, never>) => void;
export type HKT = {
    param: unknown;
    result: unknown;
};
export type Apply<f extends HKT, x> = (f & {
    param: x;
})['result'];
export declare const dictMapDecode: (decodeTreeDict: Dict<Decoder>, options: DecoderOptions) => Dict<DecoderFn<any>>;
export {};
