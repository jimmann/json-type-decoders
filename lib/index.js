"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DecodingContext = exports.decoder = exports.numberString = exports.date = exports.isodate = exports.map = exports.set = exports.construct = exports.constructor = exports.call = exports.callT = exports.args = exports.argsT = exports.arg = exports.ternary = exports.validate = exports.withDecoder = exports.path = exports.UP = exports.jsonValue = exports.type = exports.lookup = exports.everyO = exports.everyT = exports.every = exports.altT = exports.alt = exports.decode = exports.array = exports.dict = exports.tupleT = exports.tuple = exports.object = exports.stringLiteral = exports.literalValue = exports.constant = exports.opt = exports.def = exports.literalobjectLike = exports.literalobject = exports.literalarray = exports.literalnull = exports.boolean = exports.number = exports.string = exports.objectLike = exports.autoCreate = exports.onlyOne = exports.noExtraProps = exports.ignoringErrors = void 0;
exports.dictMapDecode = exports.checkDecoder = exports.DecodeError = exports.decoderInfo = void 0;
exports.ignoringErrors = true;
exports.noExtraProps = true;
exports.onlyOne = true;
exports.autoCreate = true;
exports.objectLike = true;
const defaultDecodingConfig = {
    maxErrorCount: 10,
};
// -------------------------------------
//
//  Decode specific JSON types
//
/**
 * Returns the input json if it is a string
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns string
 * @throws when json isn't a string
 */
const string = (json, ctx) => {
    if (typeof json === 'string')
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['string'], json);
};
exports.string = string;
/**
 * Returns the input json if it is a number
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns number
 * @throws when json isn't a number
 */
const number = (json, ctx) => {
    if (typeof json === 'number')
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['number'], json);
};
exports.number = number;
/**
 * Returns the inout json if it is a boolean
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns boolean
 * @throws when json isn't a boolean
 */
const boolean = (json, ctx) => {
    if (typeof json === 'boolean')
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['boolean'], json);
};
exports.boolean = boolean;
/**
 * Returns the input json if it is a null
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns null
 * @throws when json isn't a null
 */
const literalnull = (json, ctx) => {
    if (json === null)
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['null'], json);
};
exports.literalnull = literalnull;
/**
 * Returns the input json if it is a JS array (not recursive)
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an array (any[])
 * @throws when json isn't an array
 */
const literalarray = (json, ctx) => {
    if (Array.isArray(json))
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['array'], json);
};
exports.literalarray = literalarray;
/**
 * Returns json if it is a JS object (not recursive)
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an object
 * @throws when json isn't an objext
 */
const literalobject = (json, ctx) => {
    if (typeof json === 'object'
        && json !== null
        && Object.getPrototypeOf(json) === (Object.prototype))
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['object'], json);
};
exports.literalobject = literalobject;
/**
 * Returns json if it is objectLike (not recursive)
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an object or class
 * @throws when json isn't an objext or class
 */
const literalobjectLike = (json, ctx) => {
    if (typeof json === 'object'
        && json !== null)
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['object'], json);
};
exports.literalobjectLike = literalobjectLike;
// --------------------------------------------------------------
//
// Default / optional / constant / literalValue / stringLiteral
//
/**
 * Build a decoder that returns a default value if there is nothing to decode (eg missing field).
 * By default this combinator will throw if there is a JSON value present, but it fails.
 * If options.ignoringErrors is true, return the default value if the given decoder throws an exception
 *
 * @param decoder to try first
 * @param def the default value to use
 * @options .ignoringErrors
 * @returns the decoded / default value
 * @throws whatever the decoder throws
 */
const def = (decodeTree, def, options = {}) => {
    const decoder = (0, exports.decode)(decodeTree, options);
    return (json, ctx) => {
        try {
            if (json === undefined && ctx !== null)
                return def;
            return decoder(json, ctx);
        }
        catch (e) {
            if (options.ignoringErrors)
                return def;
            throw e;
        }
    };
};
exports.def = def;
/**
 * Build a decoder that returns undefined if there is nothing to decode (ie missing field in object)
 * If options.ignoringErrors is true, return the default value if the given decoder throws an exception
 *
 * @param decoder to use if the field is presnt
 * @returns the decoded value
 * @throws whatever the decoder throws
 */
const opt = (decoder, options = {}) => (0, exports.def)(decoder, undefined, options);
exports.opt = opt;
/**
 * Build a DecoderFn that ignores the provided json value at decode time, and returns the given value
 *
 * @param constantValue to return on decode
 * @returns the constantValue at decode time
 */
const constant = (constantValue) => (_json, ctx) => {
    if (ctx !== null)
        return constantValue;
    throw new DecoderInfo([], {});
};
exports.constant = constant;
/**
 * Build a DecoderFn that matches any of the provided values
 *
 * @param values array of values to check the unknown json against
 * @returns the found value
 * @throws if none of the values match
 */
const literalValue = (values) => (json, ctx) => {
    if (ctx !== null && values.indexOf(json) >= 0)
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, 'one of: [' + values.map(v => JSON.stringify(v)).join(',') + ']', json);
};
exports.literalValue = literalValue;
/**
 * Build a DecoderFn that matches one of the provided strings. The return type is a stringLiteral type
 *
 * @param strings to check the unknown json against
 * @returns the string found - a string literal.
 * @throws if none of the strings match
 */
const stringLiteral = (...strings) => (json, ctx) => {
    if (ctx !== null && strings.indexOf(json) >= 0)
        return json;
    return DecodingContext.throwTypeErrorOrInfo(ctx, 'one of: [' + strings.map(v => JSON.stringify(v)).join(',') + ']', json);
};
exports.stringLiteral = stringLiteral;
// --------------------------------------------------
//
// object / tuples / dictionaries / arrays / decode
/**
 * Build a DecoderFn that matches a JSON object where the given fields match the coresponding decoder.
 *
 * Options:
 *
 *   name: decoder name to use in errors
 *
 *   noExtraProps: any extra fields consititute an error
 *   objectLike  : accept classes and objects
 *
 * @param decoders an object of decoders that the unknown json must contain, and the decoders each value must match
 * @param options decoding options (.name & .noExtraProps)
 * @returns an object whos fields consist of the decoder results
 * @throws if any of the field decoders throw, or if there are excess fields in the unkown json
 */
const object = (decodeTreeDict, options = {}) => {
    const decoders = (0, exports.dictMapDecode)(decodeTreeDict, { ...options, name: undefined });
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, (_a = options.name) !== null && _a !== void 0 ? _a : 'object', decoders);
        const obj = json === undefined && options.autoCreate
            ? {}
            : options.objectLike ? (0, exports.literalobjectLike)(json, context) : (0, exports.literalobject)(json, context);
        const res = {};
        let decoderError;
        Object.keys(decoders).forEach(key => {
            try {
                const u = obj[key];
                res[key] = decoders[key](u, context.push(key, u));
            }
            catch (e) {
                decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount);
            }
        });
        if (decoderError)
            throw decoderError;
        if (options.noExtraProps && Object.keys(obj).length != Object.keys(res).length)
            throw context.decoderError('source object has extra fields', json);
        return res;
    };
};
exports.object = object;
/**
 * Build a DecoderFn that matches a hetrogeneous JSON array where each elements matches the
 * coresponding decoder.
 *
 * @param decoders a tuple of decoders that the unknown json must contain.
 * @returns the decoded tuple
 * @throws if any of the element decoders throw
 */
const tuple = (...decodeTrees) => (0, exports.tupleT)(decodeTrees, {});
exports.tuple = tuple;
/**
 * Build a decoder that runs all the decoders in a tuple.
 *
 * @param decodeTreeTuple tuple of decoders to try
 * @param options .name for error reporting
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
const tupleT = (decodeTreeTuple, options = {}) => {
    const decoders = decodeTreeTuple.map(p => (0, exports.decode)(p, { ...options, name: undefined }));
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, (_a = options.name) !== null && _a !== void 0 ? _a : 'tuple', decoders);
        const rawArray = (0, exports.literalarray)(json, context);
        let decoderError;
        const res = decoders.map((decoder, i) => {
            try {
                return decoder(rawArray[i], context.push(i, rawArray[i]));
            }
            catch (e) {
                decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount);
            }
        });
        if (decoderError)
            throw decoderError;
        return res;
    };
};
exports.tupleT = tupleT;
/**
 * Build a dictionary decoder - object where properties are all of the same type, and use the same decoder
 *
 * @param valueDecoder
 * @param options : options.name : name of the decoder to use in errors, safe : ignore values that fail to decode
 * @returns the decoded dictionary
 * @throws if none of the values match
 */
const dict = (valueDecoder, options = {}) => {
    const vDecoder = (0, exports.decode)(valueDecoder, { ...options, name: undefined });
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, (_a = options.name) !== null && _a !== void 0 ? _a : 'dict', { value: vDecoder });
        const dict = options.objectLike ? (0, exports.literalobjectLike)(json, context) : (0, exports.literalobject)(json, context);
        const res = {};
        let decoderError;
        Object.keys(dict).map((key) => {
            try {
                res[key] = vDecoder(dict[key], context.push(key, dict[key]));
            }
            catch (e) {
                if (options.ignoringErrors)
                    return;
                decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount);
            }
        });
        if (decoderError)
            throw decoderError;
        return res;
    };
};
exports.dict = dict;
/**
 * Build a homogeneous array decoder - an array where elements are all of the same type, and use the same decoder
 *
 * @param elementDecoder
 * @param options : options.name : name of the decoder to use in errors, safe : ignore values that fail to decode
 * @returns the decoded array
 * @throws if an element fails to decode and options.safe is not true
 */
const array = (elementDecodeTree, options = {}) => {
    const elementDecoder = (0, exports.decode)(elementDecodeTree, { ...options, name: undefined });
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, (_a = options.name) !== null && _a !== void 0 ? _a : 'array', { value: elementDecoder });
        const rawArray = (0, exports.literalarray)(json, context);
        const res = [];
        let decoderError;
        rawArray.forEach((elem, i) => {
            try {
                res.push(elementDecoder(elem, context.push(i, elem)));
            }
            catch (e) {
                if (options.ignoringErrors)
                    return;
                decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount);
            }
        });
        if (decoderError)
            throw decoderError;
        return res;
    };
};
exports.array = array;
// ------------------------------------------------------------------------------
/**
 * Build a decoder for a nested object tree of decoders.
 *
 * @param decodeTree object tree of decoders descibing struture and types
 * @param options Decoding options (name & strict)
 * @returns an object whos fields consist of the decoder results
 * @throws if any of the field decoders throw, or if there are excess fields in the unkown json

 */
const decode = (decodeTree, options = {}) => {
    switch (typeof decodeTree) {
        case 'function': return decodeTree;
        case 'object':
            const newOptions = { ...options, name: undefined };
            if (Array.isArray(decodeTree))
                return (0, exports.array)((0, exports.altT)(decodeTree, newOptions), options);
            return (0, exports.object)((0, exports.dictMapDecode)(decodeTree, newOptions), options);
        // case 'number':
        // case 'string':
        // case 'boolean':
        default:
            return (0, exports.literalValue)([decodeTree]);
    }
};
exports.decode = decode;
// --------------------------------
//
// Try all decoders until one passes / all pass
//
/**
 * Build a decoder that tries a sequence of decoders in turn until one succeeds.
 * (use altOpts if you want to use options)
 *
 * @param decoders tuple of decoders to try
 * @returns the result of the first successful decoder
 * @throws if none of the decoders match
 */
const alt = (...decodeTreeArray) => (0, exports.altT)(decodeTreeArray, {});
exports.alt = alt;
/**
 * Build a decoder (with Options) that tries an array of decoders in turn until one succeeds.
 *
 * @param options .name for error reporting
 * @param decoders tuple of decoders to try
 * @returns the result of the first successful decoder
 * @throws if none of the decoders match
 */
const altT = (altDecoders, options = {}) => {
    // Fast track singleton arrays
    if (altDecoders.length === 1)
        return altDecoders[0];
    const newOptions = { ...options, name: undefined };
    const decoders = altDecoders.map(p => (0, exports.decode)(p, newOptions));
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, (_a = options.name) !== null && _a !== void 0 ? _a : 'alternate', decoders);
        // some arrays to collect the results/failures
        const failures = [];
        const results = [];
        for (const decoder of decoders) {
            try {
                if (!options.onlyOne)
                    return decoder(json, context);
                results.push(decoder(json, context));
            }
            catch {
                failures.push(decoder);
            }
        }
        if (results.length == 1)
            return results[0];
        if (results.length == 0) {
            const expecting = new Set();
            decoders.map(p => { var _a; return (_a = decoderInfo(p)) === null || _a === void 0 ? void 0 : _a.expected.map(e => expecting.add(e)); });
            throw context.typeError([...expecting.keys()], json);
        }
        throw context.decoderError('Was expecting onlyOne of ' + decoders.length + ' decoders to match', json);
    };
};
exports.altT = altT;
/**
 * Build a decoder that runs all the decoders in a tuple. They all have to pass or an error is thrown.
 * Each decoder is passed in the top level json
 *
 * @param decodeTreeArray tuple of decoders to try
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
const every = (...decodeTreeArray) => (0, exports.everyT)(decodeTreeArray, {});
exports.every = every;
/**
 * Build a decoder that runs all the decoders in a tuple. They all have to pass or an error is thrown.
 * Each decoder is passed in the same json
 *
 * @param decodeTreeArray tuple of decoders to try
 * @param options .name for error reporting
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
const everyT = (decodeTreeArray, options = {}) => {
    const newOptions = { ...options, name: undefined };
    const decoders = decodeTreeArray.map(p => (0, exports.decode)(p, newOptions));
    return (json, ctx) => {
        var _a;
        if (ctx === null)
            throw new DecoderInfo([(_a = options.name) !== null && _a !== void 0 ? _a : 'every'], decoders);
        const context = DecodingContext.create(json, ctx, options.name, decoders);
        let decoderError;
        const res = decoders.map(decoder => {
            try {
                return decoder(json, context);
            }
            catch (e) {
                decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount);
            }
        });
        if (decoderError)
            throw decoderError;
        return res;
    };
};
exports.everyT = everyT;
/**
 * Build a decoder that runs all the decoders in an object. They all have to pass or an error is thrown.
 * Each decoder is passed in the top level json
 *
 * @param decoders object of decoders to try
 * @param options .name for error reporting
 * @returns an object of the decoder results
 * @throws if any of the decoders throw
 */
const everyO = (decodeTreeDict, options = {}) => {
    const decoders = (0, exports.dictMapDecode)(decodeTreeDict, { ...options, name: undefined });
    return (json, ctx) => {
        var _a;
        if (ctx === null)
            throw new DecoderInfo([(_a = options.name) !== null && _a !== void 0 ? _a : 'all'], decoders);
        const context = DecodingContext.create(json, ctx, options.name, decoders);
        const res = {};
        let decoderError;
        Object.keys(decoders).forEach(key => {
            try {
                res[key] = decoders[key](json, context);
            }
            catch (e) {
                decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount);
            }
        });
        if (decoderError)
            throw decoderError;
        return res;
    };
};
exports.everyO = everyO;
// -----------------------------------------
//
// select decoder based on: json type / field value
// 
/**
 * Build a decoder that gets a string from a specified field/path in the json, looks up a decoder in the decoders
 * argument and returns the result of trying that decoder.
 *
 * @param lookupPath the field/path of the string to lookup in the json
 * @param decoders the object of decoders to select one from
 * @param options options.name: name of the decoder in any error messages
 * @returns an object of the decoder results
 * @throws if the field/path does not exits, isn't a string, no matching decoder is found, or the decoder throws
 */
const lookup = (lookupPath, decodeTreeDict, options = {}) => {
    const lookupDecoder = (0, exports.path)(lookupPath, exports.string, { name: 'lookup' });
    const newOptions = { ...options, name: undefined };
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, options.name, () => {
            return new DecoderInfo(['<lookup using field: ' + lookupPath + '>'], (0, exports.dictMapDecode)(decodeTreeDict, newOptions));
        });
        const decoderName = lookupDecoder(json, context);
        const decoder = (0, exports.decode)(decodeTreeDict[decoderName], newOptions);
        if (decoder === undefined)
            throw context.decoderError((_a = options.name) !== null && _a !== void 0 ? _a : 'lookup' + 'cant lookup decoder name: ' + decoderName, json);
        return decoder(json, ctx);
    };
};
exports.lookup = lookup;
/**
 * Build a decoder that checks the JSON type of the json, looks up a decoder in the decoders
 * argument based on that type and returns the result of trying that decoder. A 'default'
 * decoder is used if the actual type decoder is missing. Throw if no decoder can be found
 *
 * @param decoders the object of decoders to lookup in
 * @param options options.name: name of the decoder in any error messages
 * @returns the result of the selected decoder
 * @throws if a decoder cannot be found, or the selected decoder throws
 */
function type(decoders, options = {}) {
    return (json, ctx) => {
        var _a, _b, _c;
        if (ctx === null)
            throw new DecoderInfo([(_a = options.name) !== null && _a !== void 0 ? _a : '<type>'], decoders);
        const expected = Object.keys(decoders).filter(t => t !== 'default');
        const context = DecodingContext.create(json, ctx, (_b = options.name) !== null && _b !== void 0 ? _b : 'type', () => new DecoderInfo(expected.sort(), decoders));
        const jtype = jsonType(json);
        if ('error' in jtype)
            throw context.typeError(expected.sort(), json);
        const decoder = (jtype.type in decoders ? decoders[jtype.type] : decoders.default);
        if (decoder === undefined)
            throw context.decoderError((_c = options.name) !== null && _c !== void 0 ? _c : 'type' + ' cant lookup decoder for type: ' + jtype.type, json);
        return decoder(json, ctx);
    };
}
exports.type = type;
/**
 * A Decoder that checks recursively that the json is a JSON value.
 * Side-effect is to deep copy the json
 *
 * @param json the value to decoder as a JSON value
 * @param ctx optional Context
 * @throws if the json contains invalid JSON (eg undefined, a Class)
*/
const jsonValue = (json, ctx) => {
    return type({
        object: (0, exports.dict)(exports.jsonValue),
        array: (0, exports.array)(exports.jsonValue),
        // remaining entries are needed to populate the correct Exception.message
        boolean: b => b,
        number: n => n,
        string: s => s,
        null: n => n,
    })(json, ctx);
};
exports.jsonValue = jsonValue;
// ----------------------------
//
//  access an element at a path location and then try the given decoder
exports.UP = null;
const normPath = (path) => {
    if (Array.isArray(path))
        return path;
    if (typeof path === 'number')
        return [path];
    const normPathRE = RegExp('\\[(\\d+)\\]|\\.?([^[\\.^]+)|\\.?(\\^)', 'g');
    let match;
    const res = [];
    while ((match = normPathRE.exec(path)) !== null) {
        if (match[1] !== undefined)
            res.push(Number(match[1]));
        else if (match[2] !== undefined)
            res.push(match[2]);
        else if (match[3] !== undefined)
            res.push(exports.UP);
    }
    return res;
};
/**
 * Build a decoder that targets a different part of the unknown JSON and attepts to use the
 * provided decoder.
 *
 * Path is a string using .field or [element] syntax or an array of components (strings /
 * numbers / null). If using an array, a null can be used to travel up the unknown JSON
 * structure.
 *
 * @param path the path to json JSON value
 * @param decoder the decoder to use
 * @param options .name : the name to use in any error messages, .autoCreate : create empty object if necessary when traversing
 * @returns the result of the decoder
 * @throws if the path cannot be travelled or the decoder throws.
 */
const path = (path, decodeTree, options = {}) => (json, ctx) => {
    const decoder = (0, exports.decode)(decodeTree, options);
    const [newRaw, newCtx] = normPath(path).reduce(([raw_, ctx_], component) => {
        if (component === exports.UP) {
            const parent = ctx_.parentContext;
            if (parent === undefined)
                throw ctx_.decoderError((options.name ? '[' + options.name + ']' : '') + 'No parent available follwing the path (' + JSON.stringify(path) + ')', raw_);
            return [parent.json === undefined && options.autoCreate ? {} : parent.json, parent];
        }
        if (raw_ === undefined && options.autoCreate)
            return [undefined, ctx_.push(component, undefined)];
        if (typeof raw_ !== 'object' || raw_ === null)
            throw ctx_.typeError(['object', 'array'], raw_);
        const next = raw_[component];
        return [next, ctx_.push(component, next)];
    }, [json, DecodingContext.create(json, ctx, options.name, () => new DecoderInfo(['<path>'], [decoder]))] // initial
    );
    return decoder(newRaw, newCtx);
};
exports.path = path;
// ------------------------------------
//
// withDecoder / validate / ternary
/**
 * Try to decode something, then pass the result to a user provided function.
 *
 * @param decoder to attempt
 * @param userFn called with the result of the decoder
 * @returns the result of the user function
 * @throws if the decoder fails / user function throws
 */
const withDecoder = (decodeTree, userFn, options = {}) => {
    const decoder = (0, exports.decode)(decodeTree, { ...options, name: undefined });
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, options.name, () => decoderInfo(decoder));
        try {
            const decoded = decoder(json, context);
            return userFn(decoded, context, json);
        }
        catch (e) {
            if (e instanceof DecodeError)
                throw e;
            if (e instanceof Error)
                throw context.decoderError('withDecoder ' + ((_a = options.name) !== null && _a !== void 0 ? _a : 'userFunction') + ' threw: ' + e.message, json);
            throw e;
        }
    };
};
exports.withDecoder = withDecoder;
/**
 * Try to decode something, pass the result to a user Validators. If all validators returns true, return
 * the results of the decoder, otherwise throw an error.
 *
 * @param decoder to attempt
 * @param validator a dict of function to validate the decoder result.
 * @param options .name: name for the error.
 * @returns the result of the decoder
 * @throws if the decoder fails / validator function fails or throws
 */
const validate = (decodeTree, validators, options = {}) => {
    const decoder = (0, exports.decode)(decodeTree, { ...options, name: undefined });
    return (json, ctx) => {
        const context = DecodingContext.create(json, ctx, options.name, () => new DecoderInfo(['<validator>'], [decoder]));
        const decoded = decoder(json, context);
        const failures = [];
        Object.keys(validators).map(k => { if (!validators[k](decoded))
            failures.push(k); });
        if (failures.length == 0)
            return decoded;
        throw context.decoderError((options.name ? options.name + ' validator' : 'validation') + ' failed (with: ' + failures.join(', ') + ')', json);
    };
};
exports.validate = validate;
/**
 * At decode time, try the 'testDecoder', if that succeeds, then use 'passDecoder', otherwise use 'failDecoder' (if provided)
 *
 * @param testDecoder test decoder, any successful results are not used
 * @param passDecoder the decoder to use if the test decoder was successful
 * @param failDecoder the decoder to use if the test decoder fails
 *
 * @returns an object of the decoder results
 * @throws
 */
const ternary = (testDecodeTree, passDecodeTree, failDecodeTree, options = {}) => {
    const testDecoder = (0, exports.decode)(testDecodeTree, { ...options, name: undefined });
    const passDecoder = (0, exports.decode)(passDecodeTree, { ...options, name: undefined });
    const failDecoder = failDecodeTree != undefined ? (0, exports.decode)(failDecodeTree, { ...options, name: undefined }) : undefined;
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, options.name, () => { var _a; return new DecoderInfo([(_a = options.name) !== null && _a !== void 0 ? _a : '<ternary>'], { testDecoder, passDecoder, failDecoder }); });
        try {
            testDecoder(json, context);
        }
        catch {
            if (failDecoder)
                return failDecoder(json, context);
            throw context.decoderError(((_a = options.name) !== null && _a !== void 0 ? _a : '') + 'ternary: no failDecoder', json);
        }
        return passDecoder(json, ctx);
    };
};
exports.ternary = ternary;
// -------------------------------
//
//  arg / argsT / args , callT / call , constructor
//
/**
 *
 * @param argSpec
 * @param options
 */
const arg = (argSpec, options = {}) => {
    var _a;
    const argType = typeof argSpec;
    switch (argType) {
        case 'function': return argSpec;
        case 'object':
            const newOptions = { ...options, name: undefined };
            const decoders = Object.keys(argSpec).map(p => (0, exports.path)([p], (0, exports.arg)(argSpec[p], newOptions), newOptions));
            return (0, exports.altT)(decoders, { ...options, onlyOne: exports.onlyOne, name: (_a = options.name) !== null && _a !== void 0 ? _a : 'argument' });
        case 'number':
        case 'string':
        case 'boolean':
            return (0, exports.literalValue)([argSpec]);
        default:
            throw Error('argSpec passed an illegal ArgDecoder of type: ' + argType);
    }
};
exports.arg = arg;
const argsT = (argSpecs, options = {}) => {
    const newOptions = { ...options, name: undefined };
    return (0, exports.everyT)(argSpecs.map(da => (0, exports.arg)(da, newOptions)), options);
};
exports.argsT = argsT;
const args = (...argSpecs) => (0, exports.argsT)(argSpecs);
exports.args = args;
// --------------------------------------------
const callT = (userFn, argumnetDecoders, options = {}) => {
    const decoder = (0, exports.argsT)(argumnetDecoders, { ...options, name: undefined });
    return (json, ctx) => {
        var _a;
        const context = DecodingContext.create(json, ctx, options.name, () => decoderInfo(decoder));
        try {
            const argsT = decoder(json, context);
            return userFn(...argsT);
        }
        catch (e) {
            if (e instanceof DecodeError)
                throw e;
            if (e instanceof Error)
                throw context.decoderError('withDecoder ' + ((_a = options.name) !== null && _a !== void 0 ? _a : 'userFunction') + ' threw: ' + e.message, json);
            throw e;
        }
    };
};
exports.callT = callT;
const call = (userFn, ...argumnetDecoders) => (0, exports.callT)(userFn, argumnetDecoders, {});
exports.call = call;
const constructor = (clazz) => (...argsT) => new clazz(...argsT);
exports.constructor = constructor;
const construct = (clazz, ...argumnetDecoders) => (0, exports.callT)((0, exports.constructor)(clazz), argumnetDecoders, { name: clazz.name });
exports.construct = construct;
// -------------------------------
//
//  Set  / Map / Date / numberString
//
/**altT
 * Build a decoder that returns a Set created from a JSON array.
 *
 * @param decoder for each element in the Set: number, string, boolean or null
 * @param options .name & .safe
 * @returns a Set where each member was successfully decoded
 * @throws if a member failed to decode and options.safe is NOT true.
 */
const set = (decoder, options = {}) => {
    const newOptions = { ...options, name: undefined };
    return (0, exports.callT)(a => new Set(a), [(0, exports.array)(decoder, newOptions)], options);
};
exports.set = set;
/**
 * Build a decoder that returns a Map created from:

 * a) an object,
 * b) an array of tuples
 * c) an array of objects with 2 fields: default 'key' and 'value'.
 *
 * Both the keys and values will be decoded by the respective decoders.
 *
 * @param keyDecoder for each key: string or number
 * @param valueDecoder for each value
 * @param options .name & .safe
 * @returns a Map where each kay & value was successfully decoded
 * @throws if a key or value failed to decode and options.safe is NOT true.
 */
const map = (keyDecoder, valueDecoder, options = {}) => {
    var _a, _b;
    const newOptions = { ...options, name: undefined };
    const vDecoder = (0, exports.decode)(valueDecoder, newOptions);
    const kDecoder = (0, exports.decode)(keyDecoder, newOptions);
    // create a decoder for key-value tuples. can decode arrays (ie tuples) or
    // objects with {key,value} fields (named as per options).
    const decodeKV = type({
        array: (0, exports.tuple)(kDecoder, vDecoder),
        object: (0, exports.every)((0, exports.path)((_a = options.keyPath) !== null && _a !== void 0 ? _a : ['key'], kDecoder), (0, exports.path)((_b = options.valuePath) !== null && _b !== void 0 ? _b : ['value'], vDecoder)),
    });
    return (0, exports.withDecoder)(type({
        array: (0, exports.array)(decodeKV, { ...options, name: undefined }),
        object: (0, exports.withDecoder)((0, exports.dict)(vDecoder, { ...options, name: undefined }), (dictionary, ctx, _target) => Object.keys(dictionary).map((key) => [kDecoder(key, ctx.push(key, dictionary)), dictionary[key]])),
    }), kvs => new Map(kvs));
};
exports.map = map;
const isoDateFormat = /(\d{4})-(\d{2})-(\d{2})[T ](\d{2}):(\d{2}):(\d{2})\.?(\d{0,3})/;
/**
 * Decodes ISO 8601 date format into a JS Date Class,
 *
 * @param json unknown data to decode
 * @param ctx optional Context
 * @returns a Date class
 * @throws if a Date cannot be created.
 */
exports.isodate = (0, exports.withDecoder)(exports.string, (s, ctx, json) => {
    var _a;
    const parts = s.match(isoDateFormat);
    if (parts) {
        return new Date(Date.UTC(+parts[1], +parts[2] - 1, +parts[3], +parts[4], +parts[5], +parts[6], +(((_a = parts[7]) !== null && _a !== void 0 ? _a : '0') + '00').substring(0, 3)));
    }
    return DecodingContext.throwTypeErrorOrInfo(ctx, 'a conforming iso8601 string', json);
}, { name: 'isoDate string' });
/**
 * Decodes an number or ISO 8601 date format into a JS Date Class,
 *
 * @param json unknown data to decode
 * @param ctx optional Context
 * @returns a Date class
 * @throws if a Date cannot be created.
 */
exports.date = type({
    string: exports.isodate,
    number: (n, _ctx) => new Date(n)
}, { name: 'ISO datestring or epoch' });
/**
 * Returns the input json if it is a number string
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns number
 * @throws when json isn't a number
 */
const numberString = (json, ctx) => {
    if (typeof json === 'string') {
        const n = Number(json);
        if (!isNaN(n))
            return n;
        if (json.toLowerCase() === 'nan')
            return NaN;
    }
    return DecodingContext.throwTypeErrorOrInfo(ctx, ['numberString'], json);
};
exports.numberString = numberString;
//
// -------------------------------------
//
/**
 * Utility function to create a decoder from a simple function without needing to worry about context handling.
 * @param userFn
 */
function decoder(userFn, decoderName = 'UserDecoder') {
    return (json, ctx) => {
        const context = DecodingContext.create(json, ctx, decoderName, () => new DecoderInfo([decoderName], undefined));
        try {
            return userFn(json, context);
        }
        catch (e) {
            if (e instanceof Error)
                throw context.decoderError(decoderName + ' threw: \'' + e.message + '\'', json);
            throw e;
        }
    };
}
exports.decoder = decoder;
function assertNever(_v) { throw new Error('should never happen'); }
function jsonType(json) {
    const jtype = typeof json;
    switch (jtype) {
        case 'number':
        case 'string':
        case 'boolean': return { type: jtype };
        case 'object':
            if (json === null)
                return { type: 'null' };
            if (Array.isArray(json))
                return { type: 'array' };
            if (Object.getPrototypeOf(json) === (Object.prototype))
                return { type: 'object' };
            return { error: 'class: ' + Object.getPrototypeOf(json).constructor.name };
        case 'undefined':
        case 'symbol':
        case 'bigint':
        case 'function': return { error: jtype };
        default: assertNever(jtype);
    }
}
const startsWithVowel = RegExp('^[aeiuo]', 'i');
const startsWithLetter = RegExp('^[a-z]', 'i');
function jsonTypePretty(name, json) {
    const jtype = jsonType(json);
    return 'type' in jtype
        ? indefiniteArticle(name !== null && name !== void 0 ? name : jtype.type) + snippet(json)
        : 'invalid JSON (' + jtype.error + ')';
}
function snippet(json) {
    switch (typeof json) {
        case 'number': return ' (' + json.toString() + ')';
        case 'string': return ' ("' + (json.length < 20 ? json : json.slice(0, 20) + '...') + '")';
        case 'boolean': return ' (' + json.toString() + ')';
        default: return '';
    }
}
const indefiniteArticle = (s) => s.match(startsWithVowel)
    ? 'an ' + s
    : s.match(startsWithLetter)
        ? 'a ' + s
        : s;
class DecodingContext {
    constructor(parentContext, name, field, json, decodingConfig) {
        this.parentContext = parentContext;
        this.name = name;
        this.field = field;
        this.json = json;
        this.decodingConfig = decodingConfig;
    }
    static create(json, contextOrConfig, name, onNull) {
        if (contextOrConfig instanceof DecodingContext)
            return contextOrConfig;
        if (contextOrConfig === null)
            throw (typeof onNull === 'function') ? onNull(name) : new DecoderInfo(name ? [name] : [], onNull);
        if (contextOrConfig == undefined)
            return new DecodingContext(undefined, name, undefined, json, defaultDecodingConfig);
        return new DecodingContext(undefined, name, undefined, json, contextOrConfig);
    }
    push(field, json) {
        return new DecodingContext(this, undefined, field, json, this.decodingConfig);
    }
    static expectedList(expected) {
        return typeof expected === 'string'
            ? expected
            : expected.length > 1
                ? expected.slice(0, -1).map(indefiniteArticle).join(', ')
                    + ' or ' + indefiniteArticle(expected.slice(-1)[0])
                : expected.map(indefiniteArticle).join('');
    }
    static throwTypeErrorOrInfo(ctx, expected, json) {
        throw DecodingContext.create(json, ctx, undefined, () => new DecoderInfo(typeof expected === 'string' ? [expected] : expected, undefined)).typeError(expected, json);
    }
    typeError(expected, json) {
        throw new DecodeError(() => [
            'TypeError:',
            (this.name !== undefined && this.name !== (typeof expected === 'string' ? expected : expected[0]))
                ? ('Whilist decoding ' + indefiniteArticle(this.name) + ' got')
                : 'Got',
            jsonTypePretty(undefined, json),
            'but was expecting',
            DecodingContext.expectedList(expected),
        ].join(' '), this);
    }
    decoderError(message, json) {
        return new DecodeError(() => [
            'DecodeError:',
            message,
            'whilst decoding',
            jsonTypePretty(this.name, json),
        ].join(' '), this);
    }
}
exports.DecodingContext = DecodingContext;
class DecoderInfo extends Error {
    constructor(expected, decoders) {
        super();
        this.expected = expected;
        this.decoders = decoders;
    }
}
function decoderInfo(decoder) {
    try {
        // try to trigger a DecoderInfo exception
        decoder(undefined, null);
    }
    catch (e) {
        if (e instanceof DecoderInfo)
            return e;
    }
    return undefined;
}
exports.decoderInfo = decoderInfo;
class DecodeError extends Error {
    constructor(errorFn, ctx) {
        super();
        this.ctx = ctx;
        this.errors = [[errorFn, ctx]];
    }
    addError(decoderError) {
        this.errors.push(...decoderError.errors);
        return this;
    }
    errorCount() { return this.errors.length; }
    get messages() {
        return this.errors.map(([errorFn, ctx]) => {
            const [name, pathParts, root] = DecodeError.namePathRootValue(ctx);
            if (pathParts.length === 0)
                return errorFn();
            const prefix = name !== null && name !== void 0 ? name : (typeof pathParts[0] === 'number' ? 'array' : 'object');
            const path = pathParts.map(e => typeof e === 'number' ? '[' + e + ']' : '.' + e).join('');
            return `${errorFn()} at ${prefix}${path}`;
        });
    }
    get message() {
        const [_name, _pathParts, value] = DecodeError.namePathRootValue(this.ctx);
        const jsonText = value !== undefined ? JSON.stringify(value, undefined, 2) : 'undefined';
        const json1k = jsonText.length > 1000 ? jsonText.substr(0, 1000) + '...' : jsonText;
        return this.messages.join('\n') + '\nin: ' + json1k;
    }
    static namePathRootValue(ctx) {
        const path = [];
        while (true) {
            if (ctx.parentContext === undefined || ctx.field == undefined) {
                return [ctx.name, path, ctx.json];
            }
            path.unshift(ctx.field);
            ctx = ctx.parentContext;
        }
    }
}
exports.DecodeError = DecodeError;
const accumulateErrors = (decoderError, exception, maxErrorCount) => {
    if (exception instanceof DecodeError) {
        const newError = decoderError === undefined ? exception : decoderError.addError(exception); // start accumulating decoder errors 
        if (newError.errorCount() > maxErrorCount)
            throw newError; // throw once we've enough of them.
        return newError;
    }
    else {
        throw exception;
    } // re-throw non-decode errors
};
const checkDecoder = (error) => { };
exports.checkDecoder = checkDecoder;
// const objectMap = <IN extends { [z: string]: any } , FN extends HKT,>(
//   obj: IN,
//   // fn: FN,
//   fn: (a: IN[string]) => Apply<FN,IN[string]>,
// ): { [K in keyof IN]: Apply<FN,IN[K]> }=> {
//   const res : any = {}
//   Object.keys(obj).forEach(key => {
//     res[key] = fn(obj[key])
//   })
//   return res
// }
const dictMap = (obj, fn) => {
    const res = {};
    Object.keys(obj).forEach(key => {
        res[key] = fn(obj[key], key);
    });
    return res;
};
const dictMapDecode = (decodeTreeDict, options) => dictMap(decodeTreeDict, e => (0, exports.decode)(e, options));
exports.dictMapDecode = dictMapDecode;
//# sourceMappingURL=index.js.map