/**
 * DecodingContext is used for explaining any decode errors
 * null is used to trigger an exception
 */
type Context = DecodingContext | DecodingConfig | null

// helper Type functions

// Find the union type of all the types in a tuple
type TupleUnion<T extends ReadonlyArray<unknown>> = T[number]
// Find the union type of all the properties of an Object
type ObjectPropertiesUnion<O extends object> = O[keyof O]
// Get the elememt type from an ReadonlyArray
type ArrayMember<T> = T extends ReadonlyArray<infer U> ? U : never

/**
 * The Type of a function that decodes (generally unknown) IN data into a known type OUT 
 */
export type DecoderFn<OUT, IN = any> = (unknown: IN, ...args: any) => OUT

type UnDecoderFn<DECODER> = DECODER extends Decoder ? UnDecoder<DECODER> : never

// Map UnDecoder over the properties of an Object
type ObjectMapUnDecoderFn<DECODERS> = { [K in keyof DECODERS]: UnDecoderFn<DECODERS[K]> }

// A type structure used to describe a (basic) decoder
export type Decoder<SOURCE = any> =
  | Array<Decoder>
  | { [key: string]: Decoder }
  | DecoderFn<any, SOURCE>
  | number | string | boolean


// Find the resulting type from a (basic) decoder
type UnDecoder<DT extends Decoder>
  = DT extends DecoderFn<infer T> ? T
  // A union is needed here as we biuld an altT decoder
  : DT extends Array<Decoder> ? Array<UnionMapUnDecoder<TupleUnion<DT>>>
  : DT extends { [key: string]: Decoder } ? { [K in keyof DT]: UnDecoder<DT[K]> }
  : DT extends string | number | boolean | null ? DT
  : never

// Map UnDecoder over the members of a Union Type
type UnionMapUnDecoder<DT extends Decoder> = DT extends DT ? UnDecoder<DT> : never

// Map UnDecoder over the members of a Tuple Type
type TupleMapUnDecoder<A extends ReadonlyArray<Decoder<any>>> = { [K in keyof A]: (A[K] extends Decoder<any> ? UnDecoder<A[K]> : never) }

// A type structure used to describe a (basic) argument decoder : a path & (typically) primitive type
export type ArgDecoder =
  | DecoderFn<any>
  | { [key: string]: ArgDecoder }
  | string | number | boolean

// Find the resulting type from a (basic) argument decoder
type UnArgDecoder<DA extends ArgDecoder>
  = DA extends DecoderFn<infer T> ? T
  : DA extends { [key: string]: ArgDecoder } ? ObjectPropertiesUnion<{ [K in keyof DA]: UnArgDecoder<DA[K]> }>
  : DA extends (string | number | boolean) ? DA
  : never

// Map UnArgDecoder over the members of a Tuple Type
type TupleMapUnArgDecoder<DAs extends ArgDecoder[]> = { [K in keyof DAs]: DAs[K] extends ArgDecoder ? UnArgDecoder<DAs[K]> : never }


/**
 * An object where the property values are all of the same type.
 */
export type Dict<VALUE> = { [_key: string]: VALUE }
type UnDict<DICT> = DICT extends Dict<infer VALUE> ? VALUE : never

/**
 * DecoderOptions that change the way some decoders behave.
 */
export type DecoderOptions = Partial<{
  name: string,             // the name of the decoder
  ignoringErrors: boolean,  // ignore decoding exceptions when decoding arrays, sets & maps
  noExtraProps: boolean,    // check that an object doesn't contain any extra fields
  onlyOne: boolean,         // only One decoder should succeed in alt or altT
  keyPath: PathSpec,        // option for the map decoder
  valuePath: PathSpec,      // option for the map decoder
  autoCreate: boolean,      // build deeply nested objects
  objectLike: boolean,      // accept classes and objects
}>

export const ignoringErrors = true
export const noExtraProps = true
export const onlyOne = true
export const autoCreate = true
export const objectLike = true

type DecodingConfig = {
  maxErrorCount: number
}

const defaultDecodingConfig: DecodingConfig = {
  maxErrorCount: 10,
}

// -------------------------------------
//
//  Decode specific JSON types
//

/**
 * Returns the input json if it is a string
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns string
 * @throws when json isn't a string
 */
export const string: DecoderFn<string> = (json: unknown, ctx?: Context): string => {
  if (typeof json === 'string') return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['string'], json)
}

/**
 * Returns the input json if it is a number
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns number
 * @throws when json isn't a number
 */
export const number: DecoderFn<number> = (json: unknown, ctx?: Context): number => {
  if (typeof json === 'number') return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['number'], json)
}

/**
 * Returns the inout json if it is a boolean
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns boolean
 * @throws when json isn't a boolean
 */
export const boolean: DecoderFn<boolean> = (json: unknown, ctx?: Context): boolean => {
  if (typeof json === 'boolean') return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['boolean'], json)
}

/**
 * Returns the input json if it is a null
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns null
 * @throws when json isn't a null
 */
export const literalnull: DecoderFn<null> = (json: unknown, ctx?: Context): null => {
  if (json === null) return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['null'], json)
}

/**
 * Returns the input json if it is a JS array (not recursive)
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an array (any[])
 * @throws when json isn't an array
 */
export const literalarray: DecoderFn<Array<any>> = (json: unknown, ctx?: Context): Array<any> => {
  if (Array.isArray(json)) return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['array'], json)
}

/**
 * Returns json if it is a JS object (not recursive)
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an object
 * @throws when json isn't an objext
 */
export const literalobject: DecoderFn<Dict<any>> = (json: unknown, ctx?: Context): Dict<any> => {
  if (typeof json === 'object'
    && json !== null
    && Object.getPrototypeOf(json) === (Object.prototype)) return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['object'], json)
}

/**
 * Returns json if it is objectLike (not recursive)
 * 
 * @param json to be decoded
 * @param ctx optional Context
 * @returns an object or class
 * @throws when json isn't an objext or class
 */
export const literalobjectLike: DecoderFn<Dict<any>> = (json: unknown, ctx?: Context): Dict<any> => {
  if (typeof json === 'object'
    && json !== null) return json
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['object'], json)
}

// --------------------------------------------------------------
//
// Default / optional / constant / literalValue / stringLiteral
//

/**
 * Build a decoder that returns a default value if there is nothing to decode (eg missing field).
 * By default this combinator will throw if there is a JSON value present, but it fails.
 * If options.ignoringErrors is true, return the default value if the given decoder throws an exception
 * 
 * @param decoder to try first 
 * @param def the default value to use
 * @options .ignoringErrors
 * @returns the decoded / default value
 * @throws whatever the decoder throws
 */

export const def = <DT extends Decoder, DEF extends UnDecoder<DT>>(
  decodeTree: DT,
  def: DEF,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DT>> => {

  const decoder = decode(decodeTree, options)

  return (json: unknown, ctx?: Context): UnDecoder<DT> => {

    try {
      if (json === undefined && ctx !== null) return def
      return decoder(json, ctx)
    } catch (e) {
      if (options.ignoringErrors) return def
      throw e
    }
  }
}

/**
 * Build a decoder that returns undefined if there is nothing to decode (ie missing field in object)
 * If options.ignoringErrors is true, return the default value if the given decoder throws an exception
 * 
 * @param decoder to use if the field is presnt
 * @returns the decoded value
 * @throws whatever the decoder throws
 */
export const opt = <DT extends Decoder>(
  decoder: DT,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DT> | undefined> => def(decoder, undefined as UnDecoder<DT>, options)

/**
 * Build a DecoderFn that ignores the provided json value at decode time, and returns the given value
 * 
 * @param constantValue to return on decode
 * @returns the constantValue at decode time
 */
export const constant = <T>(constantValue: T): DecoderFn<T> => (
  _json: unknown, ctx?: Context
): T => {
  if (ctx !== null) return constantValue
  throw new DecoderInfo([], {})
}

/**
 * Build a DecoderFn that matches any of the provided values
 * 
 * @param values array of values to check the unknown json against
 * @returns the found value
 * @throws if none of the values match
 */
export const literalValue = <T extends ReadonlyArray<string | number | boolean | null>>(values: T): DecoderFn<ArrayMember<T>> => (
  json: unknown, ctx?: Context
): ArrayMember<T> => {
  if (ctx !== null && values.indexOf(json as any) >= 0) return json as ArrayMember<T>
  return DecodingContext.throwTypeErrorOrInfo(ctx, 'one of: [' + values.map(v => JSON.stringify(v)).join(',') + ']', json)
}

/**
 * Build a DecoderFn that matches one of the provided strings. The return type is a stringLiteral type
 * 
 * @param strings to check the unknown json against
 * @returns the string found - a string literal.
 * @throws if none of the strings match
 */
export const stringLiteral = <T extends string>(...strings: ReadonlyArray<T>): DecoderFn<T> => (
  json: unknown, ctx?: Context
): T => {
  if (ctx !== null && strings.indexOf(json as any) >= 0) return json as T
  return DecodingContext.throwTypeErrorOrInfo(ctx, 'one of: [' + strings.map(v => JSON.stringify(v)).join(',') + ']', json)
}

// --------------------------------------------------
//
// object / tuples / dictionaries / arrays / decode

/**
 * Build a DecoderFn that matches a JSON object where the given fields match the coresponding decoder.
 * 
 * Options:
 * 
 *   name: decoder name to use in errors
 * 
 *   noExtraProps: any extra fields consititute an error
 *   objectLike  : accept classes and objects
 * 
 * @param decoders an object of decoders that the unknown json must contain, and the decoders each value must match
 * @param options decoding options (.name & .noExtraProps)
 * @returns an object whos fields consist of the decoder results
 * @throws if any of the field decoders throw, or if there are excess fields in the unkown json
 */
export const object = <DPT extends Dict<Decoder>>(
  decodeTreeDict: DPT,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DPT>> => {

  const decoders = dictMapDecode(decodeTreeDict, { ...options, name: undefined })

  return (json: unknown, ctx?: Context): UnDecoder<DPT> => {
    const context = DecodingContext.create(json, ctx, options.name ?? 'object', decoders)

    const obj = json === undefined && options.autoCreate
      ? {}
      : options.objectLike ? literalobjectLike(json, context) : literalobject(json, context)

    const res: any = {}
    let decoderError: DecodeError | undefined

    Object.keys(decoders).forEach(key => {
      try {
        const u: unknown = obj[key]
        res[key] = decoders[key](u, context.push(key, u))
      }
      catch (e) {
        decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount)
      }
    })

    if (decoderError) throw decoderError

    if (options.noExtraProps && Object.keys(obj).length != Object.keys(res).length) throw context.decoderError('source object has extra fields', json)

    return res as UnDecoder<DPT>
  }
}

/**
 * Build a DecoderFn that matches a hetrogeneous JSON array where each elements matches the 
 * coresponding decoder.
 * 
 * @param decoders a tuple of decoders that the unknown json must contain.
 * @returns the decoded tuple
 * @throws if any of the element decoders throw
 */
export const tuple = <DTA extends any[]>(
  ...decodeTrees: DTA
): DecoderFn<TupleMapUnDecoder<DTA>> => tupleT(decodeTrees, {})

/**
 * Build a decoder that runs all the decoders in a tuple.
 * 
 * @param decodeTreeTuple tuple of decoders to try
 * @param options .name for error reporting
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
export const tupleT = <DTA extends Decoder[]>(
  decodeTreeTuple: DTA,
  options: DecoderOptions = {},
): DecoderFn<TupleMapUnDecoder<DTA>> => {

  const decoders: ReadonlyArray<DecoderFn<any>> = decodeTreeTuple.map(p => decode(p, { ...options, name: undefined }))

  return (json: unknown, ctx?: Context): TupleMapUnDecoder<DTA> => {

    const context = DecodingContext.create(json, ctx, options.name ?? 'tuple', decoders)
    const rawArray = literalarray(json, context)

    let decoderError: DecodeError | undefined

    const res = decoders.map(
      (decoder, i) => {
        try { return decoder(rawArray[i], context.push(i, rawArray[i])) }
        catch (e) {
          decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount)
        }
      },
    ) as TupleMapUnDecoder<DTA>

    if (decoderError) throw decoderError

    return res
  }
}

/**
 * Build a dictionary decoder - object where properties are all of the same type, and use the same decoder
 * 
 * @param valueDecoder 
 * @param options : options.name : name of the decoder to use in errors, safe : ignore values that fail to decode
 * @returns the decoded dictionary
 * @throws if none of the values match
 */
export const dict = <DT extends Decoder>(
  valueDecoder: DT,
  options: DecoderOptions = {},
): DecoderFn<Dict<UnDecoder<DT>>> => {

  const vDecoder = decode(valueDecoder, { ...options, name: undefined })

  return (json: unknown, ctx?: Context): Dict<UnDecoder<DT>> => {

    const context = DecodingContext.create(json, ctx, options.name ?? 'dict', { value: vDecoder })

    const dict = options.objectLike ? literalobjectLike(json, context) : literalobject(json, context)

    const res: Dict<UnDecoder<DT>> = {}
    let decoderError: DecodeError | undefined

    Object.keys(dict).map((key) => {
      try {
        res[key] = vDecoder((<any>dict)[key], context.push(key, (<any>dict)[key]))
      } catch (e) {
        if (options.ignoringErrors) return
        decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount)
      }
    })

    if (decoderError) throw decoderError

    return res
  }
}
/**
 * Build a homogeneous array decoder - an array where elements are all of the same type, and use the same decoder
 * 
 * @param elementDecoder 
 * @param options : options.name : name of the decoder to use in errors, safe : ignore values that fail to decode
 * @returns the decoded array
 * @throws if an element fails to decode and options.safe is not true
 */
export const array = <DT extends Decoder>(
  elementDecodeTree: DT,
  options: DecoderOptions = {},
): DecoderFn<Array<UnDecoder<DT>>> => {

  const elementDecoder = decode(elementDecodeTree, { ...options, name: undefined })

  return (json: unknown, ctx?: Context): Array<UnDecoder<DT>> => {

    const context = DecodingContext.create(json, ctx, options.name ?? 'array', { value: elementDecoder })

    const rawArray = literalarray(json, context)

    const res: Array<UnDecoder<DT>> = []
    let decoderError: DecodeError | undefined

    rawArray.forEach((elem, i) => {
      try {
        res.push(elementDecoder(elem, context.push(i, elem)))
      } catch (e) {
        if (options.ignoringErrors) return
        decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount)
      }
    })

    if (decoderError) throw decoderError

    return res
  }
}
// ------------------------------------------------------------------------------

/**
 * Build a decoder for a nested object tree of decoders.
 * 
 * @param decodeTree object tree of decoders descibing struture and types
 * @param options Decoding options (name & strict)
 * @returns an object whos fields consist of the decoder results
 * @throws if any of the field decoders throw, or if there are excess fields in the unkown json

 */
export const decode = <DT extends Decoder>(
  decodeTree: DT,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DT>> => {

  switch (typeof decodeTree) {
    case 'function': return decodeTree as DecoderFn<UnDecoder<DT>>
    case 'object':
      const newOptions = { ...options, name: undefined }
      if (Array.isArray(decodeTree)) return array(altT(decodeTree as Decoder[], newOptions), options) as DecoderFn<UnDecoder<DT>>
      return object(dictMapDecode(decodeTree as Dict<DecoderFn<any>>, newOptions), options) as DecoderFn<UnDecoder<DT>>
    // case 'number':
    // case 'string':
    // case 'boolean':
    default:
      return literalValue([decodeTree as any]) as DecoderFn<UnDecoder<DT>>
  }

}

// --------------------------------
//
// Try all decoders until one passes / all pass
//

/**
 * Build a decoder that tries a sequence of decoders in turn until one succeeds.
 * (use altOpts if you want to use options)
 * 
 * @param decoders tuple of decoders to try
 * @returns the result of the first successful decoder
 * @throws if none of the decoders match
 */
export const alt = <DTA extends Decoder[]>(
  ...decodeTreeArray: DTA
): DecoderFn<UnionMapUnDecoder<ArrayMember<DTA>>> => altT(decodeTreeArray, {})


/**
 * Build a decoder (with Options) that tries an array of decoders in turn until one succeeds.
 * 
 * @param options .name for error reporting
 * @param decoders tuple of decoders to try
 * @returns the result of the first successful decoder
 * @throws if none of the decoders match
 */
export const altT = <DTA extends ReadonlyArray<Decoder>>(
  altDecoders: DTA,
  options: DecoderOptions = {},
): DecoderFn<UnionMapUnDecoder<ArrayMember<DTA>>> => {

  // Fast track singleton arrays
  if (altDecoders.length === 1) return altDecoders[0] as DecoderFn<UnionMapUnDecoder<ArrayMember<DTA>>>

  const newOptions = { ...options, name: undefined }
  const decoders = altDecoders.map(p => decode(p, newOptions))

  return (json: unknown, ctx?: Context): UnionMapUnDecoder<ArrayMember<DTA>> => {

    const context = DecodingContext.create(json, ctx, options.name ?? 'alternate', decoders)

    // some arrays to collect the results/failures
    const failures: Array<DecoderFn<UnionMapUnDecoder<ArrayMember<DTA>>>> = []
    const results: Array<UnionMapUnDecoder<ArrayMember<DTA>>> = []

    for (const decoder of decoders) {
      try {
        if (!options.onlyOne) return decoder(json, context)
        results.push(decoder(json, context))
      } catch {
        failures.push(decoder)
      }
    }

    if (results.length == 1) return results[0]

    if (results.length == 0) {

      const expecting: Set<string> = new Set()
      decoders.map(p => decoderInfo(p)?.expected.map(e => expecting.add(e)))

      throw context.typeError([...expecting.keys()], json)
    }

    throw context.decoderError('Was expecting onlyOne of ' + decoders.length + ' decoders to match', json)

  }

}

/**
 * Build a decoder that runs all the decoders in a tuple. They all have to pass or an error is thrown.
 * Each decoder is passed in the top level json
 * 
 * @param decodeTreeArray tuple of decoders to try
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
export const every = <DTA extends ReadonlyArray<any>>(
  ...decodeTreeArray: DTA
): DecoderFn<TupleMapUnDecoder<DTA>> => everyT(decodeTreeArray, {})

/**
 * Build a decoder that runs all the decoders in a tuple. They all have to pass or an error is thrown.
 * Each decoder is passed in the same json
 * 
 * @param decodeTreeArray tuple of decoders to try
 * @param options .name for error reporting
 * @returns a tuple of the decoder results
 * @throws if any of the decoders throw
 */
export const everyT = <DTA extends ReadonlyArray<Decoder>>(
  decodeTreeArray: DTA,
  options: DecoderOptions = {},
): DecoderFn<TupleMapUnDecoder<DTA>> => {

  const newOptions = { ...options, name: undefined }
  const decoders: ReadonlyArray<DecoderFn<any>> = decodeTreeArray.map(p => decode(p, newOptions))

  return (json: unknown, ctx?: Context): TupleMapUnDecoder<DTA> => {

    if (ctx === null) throw new DecoderInfo([options.name ?? 'every'], decoders)
    const context = DecodingContext.create(json, ctx, options.name, decoders)

    let decoderError: DecodeError | undefined

    const res = decoders.map(
      decoder => {
        try {
          return decoder(json, context)
        } catch (e) {
          decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount)
        }
      })

    if (decoderError) throw decoderError

    return res as unknown as TupleMapUnDecoder<DTA>

  }

}

/**
 * Build a decoder that runs all the decoders in an object. They all have to pass or an error is thrown.
 * Each decoder is passed in the top level json
 * 
 * @param decoders object of decoders to try
 * @param options .name for error reporting
 * @returns an object of the decoder results
 * @throws if any of the decoders throw
 */
export const everyO = <DPT extends Dict<Decoder>>(
  decodeTreeDict: DPT,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DPT>> => {

  const decoders = dictMapDecode(decodeTreeDict, { ...options, name: undefined })

  return (json: unknown, ctx?: Context): UnDecoder<DPT> => {

    if (ctx === null) throw new DecoderInfo([options.name ?? 'all'], decoders)
    const context = DecodingContext.create(json, ctx, options.name, decoders)

    const res: any = {}
    let decoderError: DecodeError | undefined

    Object.keys(decoders).forEach(key => {
      try {
        res[key] = decoders[key](json, context)
      } catch (e) {
        decoderError = accumulateErrors(decoderError, e, context.decodingConfig.maxErrorCount)
      }
    })

    if (decoderError) throw decoderError

    return res as UnDecoder<DPT>
  }
}

// -----------------------------------------
//
// select decoder based on: json type / field value
// 

/**
 * Build a decoder that gets a string from a specified field/path in the json, looks up a decoder in the decoders
 * argument and returns the result of trying that decoder.
 * 
 * @param lookupPath the field/path of the string to lookup in the json
 * @param decoders the object of decoders to select one from
 * @param options options.name: name of the decoder in any error messages
 * @returns an object of the decoder results
 * @throws if the field/path does not exits, isn't a string, no matching decoder is found, or the decoder throws
 */
export const lookup = <DPT extends Dict<Decoder>>(
  lookupPath: string | ReadonlyArray<string>,
  decodeTreeDict: DPT,
  options: DecoderOptions = {},
): DecoderFn<UnDict<UnDecoder<DPT>>> => {

  const lookupDecoder = path(lookupPath, string, { name: 'lookup' })

  const newOptions = { ...options, name: undefined }

  return (json: unknown, ctx?: Context): UnDict<UnDecoder<DPT>> => {

    const context = DecodingContext.create(json, ctx, options.name, () => {
      return new DecoderInfo(['<lookup using field: ' + lookupPath + '>'], dictMapDecode(decodeTreeDict, newOptions))
    })

    const decoderName = lookupDecoder(json, context)

    const decoder = decode(decodeTreeDict[decoderName], newOptions)

    if (decoder === undefined) throw context.decoderError(options.name ?? 'lookup' + 'cant lookup decoder name: ' + decoderName, json)

    return decoder(json, ctx)
  }
}

type TypeDecoders = {
  boolean: DecoderFn<any, boolean>,
  number: DecoderFn<any, number>,
  string: DecoderFn<any, string>,
  null: DecoderFn<any, null>,
  object: DecoderFn<any, object>,
  array: DecoderFn<any, ReadonlyArray<unknown>>,
  default: DecoderFn<any, unknown>,
}

type JSONtypes = Exclude<keyof TypeDecoders, 'default'>

/**
 * Build a decoder that checks the JSON type of the json, looks up a decoder in the decoders
 * argument based on that type and returns the result of trying that decoder. A 'default'
 * decoder is used if the actual type decoder is missing. Throw if no decoder can be found
 * 
 * @param decoders the object of decoders to lookup in
 * @param options options.name: name of the decoder in any error messages
 * @returns the result of the selected decoder
 * @throws if a decoder cannot be found, or the selected decoder throws
 */
export function type<P extends Partial<TypeDecoders>>(
  decoders: P,
  options: DecoderOptions = {},
): DecoderFn<ObjectPropertiesUnion<ObjectMapUnDecoderFn<P>>> {

  return (json: unknown, ctx?: Context): ObjectPropertiesUnion<ObjectMapUnDecoderFn<P>> => {

    if (ctx === null) throw new DecoderInfo([options.name ?? '<type>'], decoders)

    const expected = Object.keys(decoders).filter(t => t !== 'default')

    const context = DecodingContext.create(json, ctx, options.name ?? 'type', () => new DecoderInfo(expected.sort(), decoders))

    const jtype = jsonType(json)
    if ('error' in jtype) throw context.typeError(expected.sort(), json)

    const decoder = (jtype.type in decoders ? decoders[jtype.type] : decoders.default) as DecoderFn<any, unknown>

    if (decoder === undefined) throw context.decoderError(options.name ?? 'type' + ' cant lookup decoder for type: ' + jtype.type, json)

    return decoder(json, ctx)

  }
}

// ---------------------------------------------

export type JsonValue =
  | string
  | number
  | boolean
  | null
  | JsonArray
  | JsonObject

export interface JsonArray extends ReadonlyArray<JsonValue> { }
export interface JsonObject { [member: string]: JsonValue }

/**
 * A Decoder that checks recursively that the json is a JSON value.
 * Side-effect is to deep copy the json
 * 
 * @param json the value to decoder as a JSON value
 * @param ctx optional Context
 * @throws if the json contains invalid JSON (eg undefined, a Class)
*/
export const jsonValue: DecoderFn<JsonValue> = (json: unknown, ctx?: Context): JsonValue => {
  return type({
    object: dict(jsonValue),
    array: array(jsonValue),
    // remaining entries are needed to populate the correct Exception.message
    boolean: b => b,
    number: n => n,
    string: s => s,
    null: n => n,
  })(json, ctx)
}

// ----------------------------
//
//  access an element at a path location and then try the given decoder

export const UP = null
export type PathSpec = ReadonlyArray<string | number | null> | number | string

const normPath = (path: PathSpec): ReadonlyArray<string | number | null> => {
  if (Array.isArray(path)) return path
  if (typeof path === 'number') return [path]

  const normPathRE = RegExp('\\[(\\d+)\\]|\\.?([^[\\.^]+)|\\.?(\\^)', 'g')

  let match: RegExpExecArray | null
  const res: Array<string | number | null> = []

  while ((match = normPathRE.exec(<string>path)) !== null) {
    if (match[1] !== undefined) res.push(Number(match[1]))
    else if (match[2] !== undefined) res.push(match[2])
    else if (match[3] !== undefined) res.push(UP)
  }
  return res
}

/**
 * Build a decoder that targets a different part of the unknown JSON and attepts to use the
 * provided decoder.
 * 
 * Path is a string using .field or [element] syntax or an array of components (strings / 
 * numbers / null). If using an array, a null can be used to travel up the unknown JSON
 * structure.
 * 
 * @param path the path to json JSON value
 * @param decoder the decoder to use
 * @param options .name : the name to use in any error messages, .autoCreate : create empty object if necessary when traversing
 * @returns the result of the decoder
 * @throws if the path cannot be travelled or the decoder throws.
 */
export const path = <DT extends Decoder>(
  path: PathSpec,
  decodeTree: DT,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DT>> => (
  json: unknown, ctx?: Context
): UnDecoder<DT> => {

    const decoder = decode(decodeTree, options)

    const [newRaw, newCtx]: [unknown, DecodingContext] = normPath(path).reduce(
      ([raw_, ctx_]: [unknown, DecodingContext], component: string | number | null): [unknown, DecodingContext] => {

        if (component === UP) {
          const parent = ctx_.parentContext
          if (parent === undefined) throw ctx_.decoderError((options.name ? '[' + options.name + ']' : '') + 'No parent available follwing the path (' + JSON.stringify(path) + ')', raw_)
          return [parent.json === undefined && options.autoCreate ? {} : parent.json, parent]
        }

        if (raw_ === undefined && options.autoCreate) return [undefined, ctx_.push(component, undefined)]
        if (typeof raw_ !== 'object' || raw_ === null) throw ctx_.typeError(['object', 'array'], raw_)
        const next = (<any>raw_)[component]
        return [next, ctx_.push(component, next)]

      },
      [json, DecodingContext.create(json, ctx, options.name, () => new DecoderInfo(['<path>'], [decoder]))] // initial
    )

    return decoder(newRaw, newCtx)

  }
// ------------------------------------
//
// withDecoder / validate / ternary

/**
 * Try to decode something, then pass the result to a user provided function.
 * 
 * @param decoder to attempt
 * @param userFn called with the result of the decoder
 * @returns the result of the user function
 * @throws if the decoder fails / user function throws
 */
export const withDecoder = <DT extends Decoder, R>(
  decodeTree: DT,
  userFn: (t: UnDecoder<DT>, ctx: DecodingContext, json: unknown) => R,
  options: DecoderOptions = {},
): DecoderFn<R> => {

  const decoder = decode(decodeTree, { ...options, name: undefined })

  return (json: unknown, ctx?: Context) => {

    const context = DecodingContext.create(json, ctx, options.name, () => decoderInfo(decoder))

    try {
      const decoded = decoder(json, context)
      return userFn(decoded, context, json)
    } catch (e) {
      if (e instanceof DecodeError) throw e
      if (e instanceof Error) throw context.decoderError('withDecoder ' + (options.name ?? 'userFunction') + ' threw: ' + e.message, json)
      throw e
    }
  }
}

type Validators<T> = Dict<(decoded: T) => boolean>

/**
 * Try to decode something, pass the result to a user Validators. If all validators returns true, return
 * the results of the decoder, otherwise throw an error.
 * 
 * @param decoder to attempt
 * @param validator a dict of function to validate the decoder result.
 * @param options .name: name for the error.
 * @returns the result of the decoder
 * @throws if the decoder fails / validator function fails or throws
 */
export const validate = <DT extends Decoder>(
  decodeTree: DT,
  validators: Validators<UnDecoder<DT>>,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<DT>> => {

  const decoder = decode(decodeTree, { ...options, name: undefined })

  return (json: unknown, ctx?: Context): UnDecoder<DT> => {

    const context = DecodingContext.create(json, ctx, options.name, () => new DecoderInfo(['<validator>'], [decoder]))

    const decoded = decoder(json, context)

    const failures: Array<string> = []
    Object.keys(validators).map(k => { if (!validators[k](decoded)) failures.push(k) })

    if (failures.length == 0) return decoded

    throw context.decoderError(
      (options.name ? options.name + ' validator' : 'validation') + ' failed (with: ' + failures.join(', ') + ')',
      json,
    )

  }
}

/**
 * At decode time, try the 'testDecoder', if that succeeds, then use 'passDecoder', otherwise use 'failDecoder' (if provided)
 * 
 * @param testDecoder test decoder, any successful results are not used
 * @param passDecoder the decoder to use if the test decoder was successful
 * @param failDecoder the decoder to use if the test decoder fails
 *  
 * @returns an object of the decoder results
 * @throws
 */
export const ternary = <DT extends Decoder, AT extends Decoder, BT extends Decoder>(
  testDecodeTree: DT,
  passDecodeTree: AT,
  failDecodeTree: BT | undefined,
  options: DecoderOptions = {},
): DecoderFn<UnDecoder<AT> | UnDecoder<BT>> => {

  const testDecoder = decode(testDecodeTree, { ...options, name: undefined })
  const passDecoder = decode(passDecodeTree, { ...options, name: undefined })
  const failDecoder = failDecodeTree != undefined ? decode(failDecodeTree, { ...options, name: undefined }) : undefined

  return (json: unknown, ctx?: Context): UnDecoder<AT> | UnDecoder<BT> => {

    const context = DecodingContext.create(json, ctx, options.name, () => new DecoderInfo([options.name ?? '<ternary>'], { testDecoder, passDecoder, failDecoder }))

    try {
      testDecoder(json, context)
    } catch {
      if (failDecoder) return failDecoder(json, context)
      throw context.decoderError((options.name ?? '') + 'ternary: no failDecoder', json)
    }
    return passDecoder(json, ctx)
  }
}

// -------------------------------
//
//  arg / argsT / args , callT / call , constructor
//

/**
 * 
 * @param argSpec 
 * @param options 
 */
export const arg = <AS extends ArgDecoder>(
  argSpec: AS,
  options: DecoderOptions = {},
): DecoderFn<UnArgDecoder<AS>> => {

  const argType = typeof argSpec

  switch (argType) {
    case 'function': return argSpec as DecoderFn<UnArgDecoder<AS>>
    case 'object':
      const newOptions = { ...options, name: undefined }

      const decoders = Object.keys(argSpec).map(
        p => path([p], arg((argSpec as any)[p], newOptions) as any, newOptions)
      )

      return altT(decoders, { ...options, onlyOne, name: options.name ?? 'argument' }) as DecoderFn<UnArgDecoder<AS>>

    case 'number':
    case 'string':
    case 'boolean':
      return literalValue([argSpec as any]) as unknown as DecoderFn<UnArgDecoder<AS>>
    default:
      throw Error('argSpec passed an illegal ArgDecoder of type: ' + argType)
  }

}

export const argsT = <ADs extends ArgDecoder[]>(
  argSpecs: ADs,
  options: DecoderOptions = {},
): DecoderFn<TupleMapUnArgDecoder<ADs>> => {

  const newOptions = { ...options, name: undefined }

  return everyT(argSpecs.map(da => arg(da, newOptions) as any), options) as DecoderFn<TupleMapUnArgDecoder<ADs>>

}

export const args = <DAs extends any[]>(
  ...argSpecs: DAs
): DecoderFn<TupleMapUnArgDecoder<DAs>> => argsT(argSpecs)

// --------------------------------------------

export const callT = <ADs extends ArgDecoder[], ARGS extends TupleMapUnArgDecoder<ADs>, R>(
  userFn: (...t: ARGS) => R,
  argumnetDecoders: ADs,
  options: DecoderOptions = {},
): DecoderFn<R> => {

  const decoder = argsT(argumnetDecoders as any, { ...options, name: undefined })

  return (json: unknown, ctx?: Context) => {

    const context = DecodingContext.create(json, ctx, options.name, () => decoderInfo(decoder))

    try {
      const argsT = decoder(json, context)
      return userFn(...argsT as any)
    } catch (e) {
      if (e instanceof DecodeError) throw e
      if (e instanceof Error) throw context.decoderError('withDecoder ' + (options.name ?? 'userFunction') + ' threw: ' + e.message, json)
      throw e
    }
  }
}

export const call = <ADs extends ArgDecoder[], ARGS extends TupleMapUnArgDecoder<ADs>, R>(
  userFn: (...t: ARGS) => R,
  ...argumnetDecoders: ADs
): DecoderFn<R> => callT(userFn, argumnetDecoders, {})


type Constructable<ARGs extends any[], CLASS> = {
  new(...argsT: ARGs): CLASS;
}

export const constructor = <ARGs extends any[], CLASS>(
  clazz: Constructable<ARGs, CLASS>
) => (...argsT: ARGs): CLASS => new clazz(...argsT)

export const construct = <ADs extends ArgDecoder[], ARGs extends TupleMapUnArgDecoder<ADs>, CLASS>(
  clazz: Constructable<ARGs, CLASS>,
  ...argumnetDecoders: ADs
): DecoderFn<CLASS> => callT(constructor(clazz), argumnetDecoders, { name: clazz.name })

// -------------------------------
//
//  Set  / Map / Date / numberString
//

/**altT
 * Build a decoder that returns a Set created from a JSON array.
 * 
 * @param decoder for each element in the Set: number, string, boolean or null
 * @param options .name & .safe
 * @returns a Set where each member was successfully decoded
 * @throws if a member failed to decode and options.safe is NOT true.
 */
export const set = <D extends Decoder>(
  decoder: D,
  options: DecoderOptions = {},
): DecoderFn<Set<UnDecoder<D>>> => {

  const newOptions = { ...options, name: undefined }

  return callT(a => new Set(a), [array(decoder, newOptions)], options)

}

/**
 * Build a decoder that returns a Map created from:

 * a) an object, 
 * b) an array of tuples
 * c) an array of objects with 2 fields: default 'key' and 'value'.
 * 
 * Both the keys and values will be decoded by the respective decoders.
 *
 * @param keyDecoder for each key: string or number
 * @param valueDecoder for each value
 * @param options .name & .safe
 * @returns a Map where each kay & value was successfully decoded
 * @throws if a key or value failed to decode and options.safe is NOT true.
 */
export const map = <KT extends Decoder, VT extends Decoder>(
  keyDecoder: KT,
  valueDecoder: VT,
  options: DecoderOptions = {},
): DecoderFn<Map<UnDecoder<KT>, UnDecoder<VT>>> => {

  const newOptions = { ...options, name: undefined }
  const vDecoder = decode(valueDecoder, newOptions)
  const kDecoder = decode(keyDecoder, newOptions)

  // create a decoder for key-value tuples. can decode arrays (ie tuples) or
  // objects with {key,value} fields (named as per options).
  const decodeKV = type({
    array: tuple(kDecoder, vDecoder),
    object: every(
      path(options.keyPath ?? ['key'], kDecoder),
      path(options.valuePath ?? ['value'], vDecoder)
    ),
  })

  return withDecoder(
    type({
      array: array(decodeKV, { ...options, name: undefined }),
      object: withDecoder(
        dict(vDecoder, { ...options, name: undefined }),
        (dictionary, ctx, _target) => Object.keys(dictionary).map(
          (key: string): [UnDecoder<KT>, UnDecoder<VT>] => [kDecoder(key, ctx.push(key, dictionary)), dictionary[key]]
        )
      ),
    }),
    kvs => new Map(kvs))

}

const isoDateFormat = /(\d{4})-(\d{2})-(\d{2})[T ](\d{2}):(\d{2}):(\d{2})\.?(\d{0,3})/

/**
 * Decodes ISO 8601 date format into a JS Date Class,
 * 
 * @param json unknown data to decode
 * @param ctx optional Context
 * @returns a Date class
 * @throws if a Date cannot be created.
 */

export const isodate = withDecoder(
  string,
  (s, ctx, json) => {

    const parts = s.match(isoDateFormat)

    if (parts) {
      return new Date(
        Date.UTC(
          +parts[1], +parts[2] - 1, +parts[3],
          +parts[4], +parts[5], +parts[6],
          +((parts[7] ?? '0') + '00').substring(0, 3)
        )
      )
    }

    return DecodingContext.throwTypeErrorOrInfo(ctx, 'a conforming iso8601 string', json)

  },
  { name: 'isoDate string' }
)

/**
 * Decodes an number or ISO 8601 date format into a JS Date Class,
 * 
 * @param json unknown data to decode
 * @param ctx optional Context
 * @returns a Date class
 * @throws if a Date cannot be created.
 */
export const date: DecoderFn<Date> = type(
  {
    string: isodate,
    number: (n, _ctx) => new Date(n)
  },
  { name: 'ISO datestring or epoch' })


/**
 * Returns the input json if it is a number string
 *
 * @param json to be decoded
 * @param ctx optional Context
 * @returns number
 * @throws when json isn't a number
 */
export const numberString: DecoderFn<number> = (json: unknown, ctx?: Context): number => {
  if (typeof json === 'string') {
    const n = Number(json)
    if (!isNaN(n)) return n
    if (json.toLowerCase() === 'nan') return NaN
  }
  return DecodingContext.throwTypeErrorOrInfo(ctx, ['numberString'], json)
}


//
// -------------------------------------
//

/**
 * Utility function to create a decoder from a simple function without needing to worry about context handling.
 * @param userFn 
 */
export function decoder<T>(
  userFn: (json: unknown, context: DecodingContext) => T,
  decoderName = 'UserDecoder',
): DecoderFn<T> {

  return (json: unknown, ctx?: Context) => {

    const context = DecodingContext.create(json, ctx, decoderName, () => new DecoderInfo([decoderName], undefined))
    try {
      return userFn(json, context)
    } catch (e) {
      if (e instanceof Error) throw context.decoderError(decoderName + ' threw: \'' + e.message + '\'', json)
      throw e
    }
  }
}

// --------------------------------------

type InfoDecoders = undefined | ReadonlyArray<DecoderFn<any, any>> | { [key: string]: DecoderFn<any, any> | undefined }

function assertNever(_v: never): never { throw new Error('should never happen') }

function jsonType<H>(
  json: unknown,
): { type: JSONtypes } | { error: string } {
  const jtype = typeof json
  switch (jtype) {
    case 'number':
    case 'string':
    case 'boolean': return { type: jtype }
    case 'object':
      if (json === null) return { type: 'null' }
      if (Array.isArray(json)) return { type: 'array' }
      if (Object.getPrototypeOf(json) === (Object.prototype)) return { type: 'object' }
      return { error: 'class: ' + Object.getPrototypeOf(json).constructor.name }
    case 'undefined':
    case 'symbol':
    case 'bigint':
    case 'function': return { error: jtype }
    default: assertNever(jtype)
  }
}

const startsWithVowel = RegExp('^[aeiuo]', 'i')
const startsWithLetter = RegExp('^[a-z]', 'i')


function jsonTypePretty(name: string | undefined, json: unknown): string {
  const jtype = jsonType(json)
  return 'type' in jtype
    ? indefiniteArticle(name ?? jtype.type) + snippet(json)
    : 'invalid JSON (' + jtype.error + ')'
}

function snippet(
  json: unknown,
): string {
  switch (typeof json) {
    case 'number': return ' (' + json.toString() + ')'
    case 'string': return ' ("' + (json.length < 20 ? json : json.slice(0, 20) + '...') + '")'
    case 'boolean': return ' (' + json.toString() + ')'
    default: return ''
  }
}

const indefiniteArticle = (s: string) => s.match(startsWithVowel)
  ? 'an ' + s
  : s.match(startsWithLetter)
    ? 'a ' + s
    : s

export class DecodingContext {
  constructor(
    readonly parentContext: DecodingContext | undefined,
    readonly name: string | undefined,
    readonly field: string | number | undefined,
    readonly json: unknown,
    public decodingConfig: DecodingConfig,
  ) { }


  static create(
    json: unknown,
    contextOrConfig: Context | DecodingConfig | undefined,
    name: string | undefined,
    onNull: InfoDecoders | ((name: string | undefined) => (DecoderInfo | undefined)),
  ): DecodingContext {
    if (contextOrConfig instanceof DecodingContext) return contextOrConfig
    if (contextOrConfig === null) throw (typeof onNull === 'function') ? onNull(name) : new DecoderInfo(name ? [name] : [], onNull)
    if (contextOrConfig == undefined) return new DecodingContext(undefined, name, undefined, json, defaultDecodingConfig)
    return new DecodingContext(undefined, name, undefined, json, contextOrConfig)
  }

  push(
    field: string | number,
    json: unknown,
  ): DecodingContext {
    return new DecodingContext(this, undefined, field, json, this.decodingConfig)
  }

  static expectedList(expected: string | ReadonlyArray<string>): string {
    return typeof expected === 'string'
      ? expected
      : expected.length > 1
        ? expected.slice(0, -1).map(indefiniteArticle).join(', ')
        + ' or ' + indefiniteArticle(expected.slice(-1)[0])
        : expected.map(indefiniteArticle).join('')
  }

  static throwTypeErrorOrInfo(
    ctx: Context | undefined,
    expected: string | ReadonlyArray<string>,
    json: unknown,
  ): never {
    throw DecodingContext.create(
      json, ctx, undefined,
      () => new DecoderInfo(typeof expected === 'string' ? [expected] : expected, undefined),
    ).typeError(expected, json)
  }

  typeError(
    expected: string | ReadonlyArray<string>,
    json: unknown,
  ): DecodeError {

    throw new DecodeError(() => [
      'TypeError:',
      (this.name !== undefined && this.name !== (typeof expected === 'string' ? expected : expected[0]))
        ? ('Whilist decoding ' + indefiniteArticle(this.name) + ' got')
        : 'Got',
      jsonTypePretty(undefined, json),
      'but was expecting',
      DecodingContext.expectedList(expected),
    ].join(' '), this)
  }

  decoderError(
    message: string,
    json: unknown,
  ): DecodeError {
    return new DecodeError(() => [
      'DecodeError:',
      message,
      'whilst decoding',
      jsonTypePretty(this.name, json),
    ].join(' '), this)
  }

}

class DecoderInfo extends Error {
  constructor(
    readonly expected: ReadonlyArray<string>,
    readonly decoders: InfoDecoders,
  ) {
    super()
  }
}

export function decoderInfo(decoder: DecoderFn<any>): DecoderInfo | undefined {
  try {
    // try to trigger a DecoderInfo exception
    decoder(undefined, null)
  } catch (e) {
    if (e instanceof DecoderInfo) return e
  }
  return undefined
}

export class DecodeError extends Error {
  private errors: [() => string, DecodingContext][]

  constructor(errorFn: () => string, private ctx: DecodingContext) {
    super()
    this.errors = [[errorFn, ctx]]
  }

  addError(decoderError: DecodeError): DecodeError {
    this.errors.push(...decoderError.errors)
    return this
  }

  errorCount(): number { return this.errors.length }

  get messages(): string[] {
    return this.errors.map(([errorFn, ctx]) => {
      const [name, pathParts, root] = DecodeError.namePathRootValue(ctx)

      if (pathParts.length === 0) return errorFn()

      const prefix = name ?? (typeof pathParts[0] === 'number' ? 'array' : 'object')
      const path = pathParts.map(e => typeof e === 'number' ? '[' + e + ']' : '.' + e).join('')
      return `${errorFn()} at ${prefix}${path}`
    })
  }

  get message(): string {

    const [_name, _pathParts, value] = DecodeError.namePathRootValue(this.ctx)

    const jsonText = value !== undefined ? JSON.stringify(value, undefined, 2) : 'undefined'
    const json1k = jsonText.length > 1000 ? jsonText.substr(0, 1000) + '...' : jsonText

    return this.messages.join('\n') + '\nin: ' + json1k

  }


  private static namePathRootValue(ctx: DecodingContext): [string | undefined, Array<number | string>, unknown] {

    const path: Array<number | string> = []

    while (true) {

      if (ctx.parentContext === undefined || ctx.field == undefined) {
        return [ctx.name, path, ctx.json]
      }

      path.unshift(ctx.field)

      ctx = ctx.parentContext
    }
  }
}

const accumulateErrors = (decoderError: DecodeError | undefined, exception: any, maxErrorCount: number): DecodeError => {
  if (exception instanceof DecodeError) {
    const newError = decoderError === undefined ? exception : decoderError.addError(exception) // start accumulating decoder errors 
    if (newError.errorCount() > maxErrorCount) throw newError                          // throw once we've enough of them.
    return newError
  } else { throw exception }                                                            // re-throw non-decode errors

}

// ------------------------------------------------------------------------------
// https://github.com/Microsoft/TypeScript/issues/27024
//

export type Equals<X, Y, T, F> =
  (<T>() => T extends X ? 1 : 2) extends
  (<T>() => T extends Y ? 1 : 2) ? T : F

export const checkDecoder = <T, P extends DecoderFn<T>>(
  error: Equals<T, UnDecoderFn<P>, string, never>,
): void => { }

// ---------------------------


// https://github.com/microsoft/TypeScript/issues/40928

export type HKT = {
  param: unknown;
  result: unknown;
}

interface NotHKT extends HKT {
  result: this['param'] extends true ? false : true;
}

interface FstHKT extends HKT {
  result: this['param'] extends [infer A, infer _] ? A : never;
}

interface ArrayHKT extends HKT {
  result: ReadonlyArray<this['param']>;
}

export type Apply<f extends HKT, x>
  = (f & { param: x })['result'];

type Test1 = Apply<NotHKT, true> // be deducedd to `false`
type Test2 = Apply<FstHKT, [string, boolean]> // be deduced to `string`
type Test3 = Apply<ArrayHKT, number> // be deduced to `number[]`


interface UnDecoderFnHKT extends HKT {
  result: UnDecoderFn<this['param']>;
}

// const objectMap = <IN extends { [z: string]: any } , FN extends HKT,>(
//   obj: IN,
//   // fn: FN,
//   fn: (a: IN[string]) => Apply<FN,IN[string]>,

// ): { [K in keyof IN]: Apply<FN,IN[K]> }=> {

//   const res : any = {}
//   Object.keys(obj).forEach(key => {
//     res[key] = fn(obj[key])
//   })
//   return res
// }

const dictMap = <A, B>(
  obj: Dict<A>,
  fn: (a: A, k: string) => B,
): Dict<B> => {

  const res: Partial<Dict<B>> = {}
  Object.keys(obj).forEach(key => {
    res[key] = fn(obj[key], key)
  })

  return res as Dict<B>
}


export const dictMapDecode = (
  decodeTreeDict: Dict<Decoder>,
  options: DecoderOptions,
): Dict<DecoderFn<any>> => dictMap(decodeTreeDict, e => decode(e, options))
