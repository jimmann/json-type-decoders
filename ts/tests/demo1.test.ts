import { decode, opt, string, boolean, def, number, alt, literalnull } from "../src/index"
import { test } from 'mocha'

test('demo 1', () => { })

// Define a decoder for Foos
export const decodeFoo = decode({         // decode an object with...
  foo: string,                            //  a string
  nested: {                               //  a nested object with
    faz: opt(number),                     //    an optional number
    fiz: [number],                        //    an array of numbers
    foz: def(boolean, true),              //    a boolean, with a deafult value used if the field is missing.
    fuz: alt(string, number, literalnull) //    either a string, number or a null. Tried in order.
  },
})

// auto-derive the type of Foo (if needed).
export type Foo = ReturnType<typeof decodeFoo>

//  type Foo = {
//    foo: string;
//    nested: {
//        faz: number | undefined;
//        fiz: number[];
//        foz: boolean;
//        fuz: string | number | null;
//    };
//  }

try {
  // using the decoder with non conforming input:
  const fooFail = decodeFoo(JSON.parse(
    '{ "foo": true, "nested": { "fiz" : [3,4,true,null], "foz": "true", "fuz": {} } }'
  ))

} catch (e) {

  // console.log('decode failed:\n' + e.message)
  //   decode failed:
  //   TypeError: Got a boolean (true) but was expecting a string at object.foo
  //   TypeError: Got a boolean (true) but was expecting a number at object.nested.fiz[2]
  //   TypeError: Got a null but was expecting a number at object.nested.fiz[3]
  //   TypeError: Got a string ("true") but was expecting a boolean at object.nested.foz
  //   TypeError: Got an object but was expecting a string, a number or a null at object.nested.fuz
  //   in: {
  //     "foo": true,
  //     "nested": {
  //       "fiz": [
  //         3,
  //         4,
  //         true,
  //         null
  //       ],
  //       "foz": "true",
  //       "fuz": {}
  //     }
  //   }

}

// Use the decoder:
const conformingJson: unknown = JSON.parse(
  '{  "foo": "Hello", "nested": { "fiz" : [3,4], "foz": true, "fuz": null } }'
)

const foo = decodeFoo(conformingJson)

// console.log('foo.nested.fiz.length =', foo.nested.fiz.length)     // (no typecheck erros)
// foo.nested.fiz.length = 2