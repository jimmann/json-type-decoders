

import {
  string, number, boolean,
  literalarray, literalobject, literalnull,
  array,
  tuple,
  object,
  autoCreate,
  lookup,
  def, opt, literalValue, constant,
  //  safe,
  altT,
  decoderInfo,
  checkDecoder,
  stringLiteral,
  // safeDef,
  validate,
  set,
  type,
  map,
  path,
  everyT,
  everyO,
  isodate,
  date,
  Dict,
  dict,
  decoder,
  withDecoder,
  ternary,
  decode,
  ignoringErrors,
  noExtraProps,
  objectLike,
  onlyOne,
  alt,
  tupleT,
  every,
  arg,
  argsT,
  args,
  callT,
  call,
  constructor,
  jsonValue,
  DecodeError,
  numberString,
} from '../src/index'
import { test } from 'mocha'
import { strict as assert } from 'assert';

function mktuple<T extends any[]>(...argsT: T): T { return argsT }

function throws(fn: () => any, match: string) {
  assert.throws(
    fn,
    err => {
      assert(err instanceof DecodeError)
      assert(err.message.indexOf(match) === 0, '\nDidn\'t find prefix : ' + match + '\nIn DecodeError: ' + err.message)
      return true
    },
    'Should have failed, matching: ' + match
  )
}

function equals(a: any, b: any) {
  assert.deepStrictEqual(a, b)
}


// -------------------------------------
//
//  Decode specific JSON types
//

test('simple Decoders', () => {
  equals(string('A string'), 'A string')
  equals(number(42), 42)
  equals(boolean(true), true)
  equals(literalarray([1, true]), [1, true])
  equals(literalobject({ field: 1 }), { field: 1 })
  throws(() => string(true), 'TypeError: Got a boolean (true) but was expecting a string')
  throws(() => number('boom'), 'TypeError: Got a string ("boom") but was expecting a number')
  throws(() => boolean(0), 'TypeError: Got a number (0) but was expecting a boolean')
  throws(() => literalarray({}), 'TypeError: Got an object but was expecting an array')
  throws(() => literalobject(null), 'TypeError: Got a null but was expecting an object')
  throws(() => literalobject(new Date()), 'TypeError: Got invalid JSON (class: Date) but was expecting an object')
  throws(() => string([]), 'TypeError: Got an array but was expecting a string\nin: []')
  throws(() => number(null), 'TypeError: Got a null but was expecting a number\nin: null')
  throws(() => boolean(42), 'TypeError: Got a number (42) but was expecting a boolean\nin: 42')
  throws(() => boolean(undefined), 'TypeError: Got invalid JSON (undefined) but was expecting a boolean\nin: undefined')
  throws(() => literalobject([1, 2]), 'TypeError: Got an array but was expecting an object\nin: [\n  1,\n  2\n]')

})

checkDecoder<string, typeof string>('')
checkDecoder<number, typeof number>('')
checkDecoder<boolean, typeof boolean>('')
checkDecoder<string, typeof string>('')
checkDecoder<Dict<any>, typeof literalobject>('')
checkDecoder<any[], typeof literalarray>('')

test('literalObjectLike Decoder', () => {
  class A { constructor(public s: string, public n: number) { } }
  const a = new A('s', 1)
  throws(() => object({ s: string, n: number })(a), 'TypeError: Got invalid JSON (class: A) but was expecting an object\nin: {\n  "s": "s",\n  "n": 1\n}')
  equals(object({ s: string, n: number }, { objectLike })(a), { s: 's', n: 1 })

  equals(path(['n'], number)(a), 1)

})



// --------------------------------------------------------------
//
// Default / optional / safe / constant / literalValue / stringLiteral
//

test('def, opt & safe Decoders', () => {

  const optDecoder = object({ s: opt(string) })
  type Opt = { s: string | undefined };
  type RT = ReturnType<typeof optDecoder>
  checkDecoder<Opt, typeof optDecoder>('')
  checkDecoder<RT, typeof optDecoder>('')
  // @ts-expect-error 
  checkDecoder<{ s: string | boolean }, typeof optDecoder>('')
  // @ts-expect-error 
  checkDecoder<{ b: string | boolean }, typeof optDecoder>('')

  const defuDecoder = object({ s: opt(string) })
  type Defu = { s: string | undefined };
  checkDecoder<Defu, typeof defuDecoder>('')

  const defdDecoder = object({ s: def(string, 'd') })
  type Defd = { s: string };
  checkDecoder<Defd, typeof defdDecoder>('')

  const defBooleans2 = def([boolean], [])
  checkDecoder<boolean[], typeof defBooleans2>('')

  const defBooleans = def([boolean], [true])
  checkDecoder<boolean[], typeof defBooleans>('')

  // @ts-expect-error
  const defBooleans3 = def([boolean], [4])

  const safeDecoder = object({ s: opt(string, { ignoringErrors }) })
  type Safe = { s: string | undefined };
  checkDecoder<Safe, typeof safeDecoder>('')

  const safeDefDecoder = object({ s: def(string, 'def', { ignoringErrors }) })
  type SafeDef = { s: string };
  checkDecoder<SafeDef, typeof safeDefDecoder>('')

  equals(optDecoder({ s: 's' }), { s: 's' })
  equals(optDecoder({}), { s: undefined })
  throws(() => optDecoder({ s: 24 }), 'TypeError: Got a number (24) but was expecting a string at object.s\nin: {\n  \"s\": 24\n}')

  equals(defuDecoder({ s: 's' }), { s: 's' })
  equals(defuDecoder({}), { s: undefined })
  throws(() => defuDecoder({ s: 42 }), 'TypeError: Got a number (42) but was expecting a string at object.s\nin: {\n  \"s\": 42\n}')

  equals(defdDecoder({ s: 's' }), { s: 's' })
  equals(defdDecoder({}), { s: 'd' })
  throws(() => defdDecoder({ s: 25 }), 'TypeError: Got a number (25) but was expecting a string at object.s\nin: {\n  \"s\": 25\n}')

  equals(safeDecoder({ s: 's' }), { s: 's' })
  equals(safeDecoder({}), { s: undefined })
  equals(safeDecoder({ s: 43 }), { s: undefined })

  equals(safeDefDecoder({ s: 's' }), { s: 's' })
  equals(safeDefDecoder({}), { s: 'def' })
  equals(safeDefDecoder({ s: 43 }), { s: 'def' })
})

test('literalValue Decoders', () => {
  checkDecoder<null, typeof literalnull>('')
  const aORb = literalValue(['a', 'b'])
  checkDecoder<string, typeof aORb>('')
  // @ts-expect-error 
  checkDecoder<number, typeof aORb>('')

  const aOR1 = literalValue(['a', 1])
  checkDecoder<string | number, typeof aOR1>('')
  // @ts-expect-error 
  checkDecoder<string | boolean | number, typeof aOR1>('')

  equals(literalnull(null), null)
  equals(aORb('a'), 'a')
  equals(aOR1(1), 1)
  equals(object({ l: aORb })({ l: 'a' }), { l: 'a' })
})

test('literalValue DecoderFn Failures', () => {
  throws(() => object({ l: literalValue(['a', 'b']) })({ l: 'c' }), 'TypeError: Got a string ("c") but was expecting one of: [\"a\",\"b\"] at object.l\nin: {\n  \"l\": \"c\"\n}')
  throws(() => literalobject(null), 'TypeError: Got a null but was expecting an object')
  throws(() => literalobject(new Date(0)), 'TypeError: Got invalid JSON (class: Date) but was expecting an object')
})

test('constant Decoders', () => {
  const constantC = constant('c')
  checkDecoder<string, typeof constantC>('')
  // @ts-expect-error 
  checkDecoder<any, typeof constantC>('')

  equals(constantC(42), 'c')
  equals(object({ l: constant(['a', 'b']) })({ l: 'a' }), { l: ['a', 'b'] })
})

test('stringLiteral Decoders', () => {
  type SL = 'foo' | 'bar' | 'baz'
  const pSL = stringLiteral('foo', 'bar', 'baz')
  checkDecoder<SL, typeof pSL>('')
  // @ts-expect-error 
  checkDecoder<string, typeof pSL>('')

  equals(pSL('foo'), 'foo')
  throws(() => pSL('dud'), 'TypeError: Got a string ("dud") but was expecting one of: [\"foo\",\"bar\",\"baz\"]\nin: \"dud\"')

})

// --------------------------------------------------
//
// object / tuples / dictionaries / arrays


test('array Decoders', () => {
  const arrayOfNumbers = array(number)
  checkDecoder<(number[]), typeof arrayOfNumbers>('')
  // @ts-expect-error 
  checkDecoder<([number]), typeof arrayOfNumbers>('')
  // @ts-expect-error 
  checkDecoder<(any[]), typeof arrayOfNumbers>('')
  // @ts-expect-error 
  checkDecoder<(unknown[]), typeof arrayOfNumbers>('')

  equals(arrayOfNumbers([1, 2, 3]), [1, 2, 3])
  equals(arrayOfNumbers([]), [])
  equals(array(arrayOfNumbers)([[1, 2, 3], [], [4, 5, 6], [7]]), [[1, 2, 3], [], [4, 5, 6], [7]])
})

test('array DecoderFn Failures', () => {
  throws(() => array(number)([1, 2, false]), 'TypeError: Got a boolean (false) but was expecting a number at array[2]\nin: [\n  1,\n  2,\n  false\n]')
  throws(() => array(boolean)(undefined), 'TypeError: Got invalid JSON (undefined) but was expecting an array\nin: undefined')
  throws(() => array(number)(42), 'TypeError: Got a number (42) but was expecting an array\nin: 42')
  throws(() => array(array(number))([[1], [[]]]), 'TypeError: Got an array but was expecting a number at array[1][0]\nin: [\n  [\n    1\n  ],\n  [\n    []\n  ]\n]')
  throws(() => array(array(number))([[1], [4, true]]), 'TypeError: Got a boolean (true) but was expecting a number at array[1][1]\nin: [\n  [\n    1\n  ],\n  [\n    4,\n    true\n  ]\n]')
})

test('array Safe Decoders', () => {
  const arraySafeOfNumbers = array(number, { ignoringErrors })
  checkDecoder<(number[]), typeof arraySafeOfNumbers>('')
  // @ts-expect-error 
  checkDecoder<([number]), typeof arrayOfNumbers>('')

  equals(arraySafeOfNumbers([1, '2', 3]), [1, 3])
  equals(array(number, { ignoringErrors })([[42]]), [])
  equals(array(array(number), { ignoringErrors })([[1, 2, ''], [3], [4, 5]]), [[3], [4, 5]])
})

test('arraySafe Decoders chain exceptions', () => {
  equals(array(u => { if (typeof u === 'number') return u; throw 'BARF' }, { ignoringErrors })([1, '2', 3]), [1, 3])
})

test('tuple Decoders', () => {
  const tupleNumberString1 = tuple(number, string)
  checkDecoder<[number, string], typeof tupleNumberString1>('')
  // @ts-expect-error 
  checkDecoder<(number | string)[], typeof tupleNumberString1>('')

  const tupleNumberString2 = tuple(number, string)
  checkDecoder<[number, string], typeof tupleNumberString2>('')

  equals(tupleNumberString1([1, 'a']), [1, 'a'])
  equals(array(tupleNumberString1)([[1, 'a'], [2, 'b']]), [[1, 'a'], [2, 'b']])

  const tupleNumberBoolean = tuple(number, boolean)
  checkDecoder<[number, boolean], typeof tupleNumberBoolean>('')
  // @ts-expect-error 
  checkDecoder<(number | boolean)[], typeof tupleNumberString1>('')

  equals(tupleNumberBoolean([1, true]), [1, true])
  equals(array(tupleNumberBoolean)([[1, true], [2, false]]), [[1, true], [2, false]])

  const t1 = array(tuple(number, string))
  checkDecoder<[number, string][], typeof t1>('')
  const t2 = array(mktuple(number, string))
  checkDecoder<(number | string)[][], typeof t2>('')
  const t3 = array([number, string])
  checkDecoder<(number | string)[][], typeof t3>('')

})

test('tuple DecoderFn Failures', () => {
  throws(() => array(tuple(number, string))([[1, 'a'], [2]]), 'TypeError: Got invalid JSON (undefined) but was expecting a string at array[1][1]\nin: [\n  [\n    1,\n    \"a\"\n  ],\n  [\n    2\n  ]\n]')
  throws(() => tuple(number, string)([1, true]), 'TypeError: Got a boolean (true) but was expecting a string at tuple[1]\nin: [\n  1,\n  true\n]')
  throws(() => tupleT(mktuple(number, string), { name: 'keyValue' })([1, true]), 'TypeError: Got a boolean (true) but was expecting a string at keyValue[1]\nin: [\n  1,\n  true\n]')

})

test('object Decoders', () => {
  const objectNB = object({ n: number, b: boolean })
  checkDecoder<{ n: number, b: boolean }, typeof objectNB>('')
  // @ts-expect-error 
  checkDecoder<any, typeof objectNB>('')

  equals(object({ n: number })({ n: 42 }), { n: 42 })
  equals(object({ n: number })({ n: 42 }), { n: 42 })
  equals(objectNB({ n: 1, b: true, s: 'extra' }), { n: 1, b: true })
  equals(object({ n: number, b: object({ b: boolean }) })({ n: 1, b: { b: true } }), { n: 1, b: { b: true } })

  const o1 = object({ a: { b: { c: { d: def(number, 42) } } } })
  throws(() => o1(undefined), "TypeError: Got invalid JSON (undefined) but was expecting an object")

  const o2 = object({ a: { b: { c: { d: def(number, 42) } } } }, { autoCreate })
  equals(o2(undefined), { a: { b: { c: { d: 42 } } } })
})

test('maxErrorCount', () => {
  const o1 = decode({ n: number, s: string })
  throws(() => o1({ n: 'foo', s: 2 }, { maxErrorCount: 10 }), 'TypeError: Got a string ("foo") but was expecting a number at object.n\nTypeError: Got a number (2) but was expecting a string at object.s\nin: {\n  "n": "foo",\n  "s": 2\n}')
})

test('dict decoders', () => {
  const good = { a: 1, b: 2, c: 3 }
  const d1 = dict(number)
  checkDecoder<Dict<number>, typeof d1>('')
  checkDecoder<{ [key: string]: number }, typeof d1>('')

  equals(d1(good), good)
  throws(() => d1({ ...good, d: true }), 'TypeError: Got a boolean (true) but was expecting a number at dict.d')

  const d2 = dict(number, { ignoringErrors })
  checkDecoder<Dict<number>, typeof d2>('')
  checkDecoder<{ [key: string]: number }, typeof d2>('')

  equals(d2(good), good)
  equals(d2({ ...good, d: true }), good)

})

test('object DecoderFn Failures', () => {
  throws(() => object({ n: number })({ n: 'hi' }), 'TypeError: Got a string ("hi") but was expecting a number at object.n\nin: {\n  \"n\": \"hi\"\n}')
  throws(() => object({ n: number, b: object({ b: boolean }) })({ n: 1, b: { b: null } }), 'TypeError: Got a null but was expecting a boolean at object.b.b\nin: {\n  \"n\": 1,\n  \"b\": {\n    \"b\": null\n  }\n}')
})

test('strict object DecoderFn Failures', () => {
  throws(() => object({ n: number }, { noExtraProps })({ n: 4, s: 'extra' }), 'DecodeError: source object has extra fields whilst decoding an object')
})

test('mixed object / array Decoders', () => {
  equals(array(object({ n: number }))([{ n: 42 }, { n: -1 }]), [{ n: 42 }, { n: -1 }])
  equals(object({ n: number, b: array(boolean) })({ n: 1, b: [true, false] }), { n: 1, b: [true, false] })
  equals(array(object({ n: number, b: array(boolean) }))([{ n: 1, b: [] }, { n: 1, b: [false] }]), [{ n: 1, b: [] }, { n: 1, b: [false] }])
})

test('decode', () => {

  const p = decode({ a: number, b: decode(decode(string)) })
  equals(p({ a: 1, b: 'b', c: 'x' }), { a: 1, b: 'b' })
  throws(() => p({ a: 'x', b: 'b' }), 'TypeError: Got a string ("x") but was expecting a number at object.a\nin: {\n  \"a\": \"x\",\n  \"b\": \"b\"\n}')


  const p2 = decode({
    t: 'foo',
    a: string,
    b: tuple(string, decode(number)),
    c: {
      c1: opt(boolean),
      c2: [number],
      c3: {
        d1:
          alt(string, decode(number))
      }
    },
  })

  type P2 = {
    t: string,
    a: string,
    b: [string, number],
    c: {
      c1: boolean | undefined,
      c2: number[]
      c3: {
        d1: string | number
      }
    }
  }

  checkDecoder<P2, typeof p2>('')

  const raw = { t: 'foo', a: 'a', b: ['b', 2], c: { c2: [1, 2, 3], c3: { d1: 's', dx: 1 }, cx: 1 } }
  const res = { t: 'foo', a: 'a', b: ['b', 2], c: { c1: undefined, c2: [1, 2, 3], c3: { d1: 's' } } }
  equals(p2(raw), res)

  const p3 = decode({ a: { b: { c: [alt(number, string)] } } }, { name: 'Baz' })
  throws(() => p3({ a: { b: { c: [1, '2', true] } } }), 'TypeError: Got a boolean (true) but was expecting a number or a string at Baz.a.b.c[2]\nin: {\n  \"a\": {\n    \"b\": {\n      \"c\": [\n        1,\n        "2",\n        true\n      ]\n    }\n  }\n}')

  const p4 = decode({ a: { b: { c: [alt(number, string)] } } }, { noExtraProps, name: 'User' })
  throws(() => p4({ a: { b: { c: [1, '2', true] } } }), 'TypeError: Got a boolean (true) but was expecting a number or a string at User.a.b.c[2]\nin: {\n  \"a\": {\n    \"b\": {\n      \"c\": [\n        1,\n        "2",\n        true\n      ]\n    }\n  }\n}')
  throws(() => p4({ x: 1, a: { b: { c: [1, '2'] } } }), 'DecodeError: source object has extra fields whilst decoding an User\nin: {\n  \"x\": 1,\n  \"a\": {\n    \"b\": {\n      \"c\": [\n        1,\n        "2"\n      ]\n    }\n  }\n}')

  const p5 = decode({ t: 'foo', a: number })
  throws(() => p5({ t: 'bax', a: 42 }), 'TypeError: Got a string (\"bax\") but was expecting one of: [\"foo\"] at object.t\nin: {\n  \"t\": \"bax\",\n  \"a\": 42\n}')

  const p6 = decode({ a: [number, decode(string, { name: 'notUsed' })] }, { name: 'Widget' })
  checkDecoder<{ a: (number | string)[] }, typeof p6>('')
  equals(p6({ a: [1, 'two', 3] }), { a: [1, 'two', 3] })
  throws(() => p6({ a: [1, 'two', true] }), 'TypeError: Got a boolean (true) but was expecting a number or a string at Widget.a[2]\nin: {\n  \"a\": [\n    1,\n    \"two\",\n    true\n  ]\n}')

})


// --------------------------------
//
// Try all decoders until one passes / all pass
//

test('path Decoders', () => {

  const p1 = decode({
    bs: [boolean],
    nbs: withDecoder(path('^.bs', literalarray, { autoCreate }), as => as.length),
    deep: {
      nbs: withDecoder(path('^.^.bs', literalarray, { autoCreate }), as => as.length),
      syn: path('^.^.dummy[2].dummy', def(string, 'yes!'), { autoCreate })
    }
  }, { autoCreate })
  checkDecoder<{ bs: boolean[], nbs: number, deep: { nbs: number, syn: string } }, typeof p1>('')

  equals(p1({ bs: [true, true] }), { bs: [true, true], nbs: 2, deep: { nbs: 2, syn: 'yes!' } })

  throws(() => path('^.nope', string)({}), 'DecodeError: No parent available follwing the path ("^.nope") whilst decoding an object')

})

test('altT Decoders', () => {

  equals(object({ n: alt(number, constant('c')), })({ n: true }), { n: 'c' })

  const alt1 = alt(number, string, boolean)
  checkDecoder<number | string | boolean, typeof alt1>('')
  // @ts-expect-error 
  checkDecoder<[number, string, boolean], typeof alt1>('')
  // @ts-expect-error 
  checkDecoder<(number | string | boolean)[], typeof alt1>('')

  equals(alt1(true), true)
  throws(() => alt1({ fail: true }), 'TypeError: Whilist decoding an alternate got an object but was expecting a number, a string or a boolean\nin: {\n  "fail": true\n}')

  const alt2 = altT(mktuple(number, string, boolean))
  checkDecoder<number | string | boolean, typeof alt2>('')
  // @ts-expect-error 
  checkDecoder<[number, string, boolean], typeof alt2>('')
  // @ts-expect-error 
  checkDecoder<(number | string | boolean)[], typeof alt2>('')

  equals(alt2(true), true)
  throws(() => alt2({ fail: true }), 'TypeError: Whilist decoding an alternate got an object but was expecting a number, a string or a boolean\nin: {\n  "fail": true\n}')

  const alt3 = altT(mktuple({ s: string }, { b: number }), { onlyOne })
  checkDecoder<{ s: string } | { b: number }, typeof alt3>('')

  equals(alt3({ s: 's', b: true }), { s: 's' })
  throws(() => alt3({ s: 's', b: 0 }), 'DecodeError: Was expecting onlyOne of 2 decoders to match whilst decoding an alternate')

  const o1 = altT(mktuple(number, string, boolean), { onlyOne })
  equals(o1(true), true)
  checkDecoder<number | string | boolean, typeof o1>('')
  // @ts-expect-error 
  checkDecoder<[number, string, boolean], typeof o1>('')
  // @ts-expect-error 
  checkDecoder<(number | string | boolean)[], typeof o1>('')

  const o2 = altT(mktuple({ a: number }, { b: string }, { c: boolean }), { onlyOne })

  equals(o2({ b: 'b', z: 0 }), { b: 'b' })
  throws(() => o2({ b: 'b', a: 0 }), 'DecodeError: Was expecting onlyOne of 3 decoders to match whilst decoding an alternate')

  throws(() => alt(number, boolean)('s'), 'TypeError: Whilist decoding an alternate got a string ("s") but was expecting a number or a boolean')

  const decodeFoo = object({ foo: string }, { name: 'Foo' })
  const a1 = alt(decodeFoo, boolean)
  throws(() => a1('s'), 'TypeError: Whilist decoding an alternate got a string ("s") but was expecting a Foo or a boolean\nin: \"s\"')

  const a2 = altT(mktuple({ a: number }, { b: string }), { name: 'aORb' })
  checkDecoder<{ a: number } | { b: string }, typeof a2>('')

  equals(a2({ b: 'b', z: 0 }), { b: 'b' })
  equals(a2({ b: 'b', a: 0 }), { a: 0 })
  throws(() => a2({ z: true }), 'TypeError: Whilist decoding an aORb got an object but was expecting an object\nin: {\n  "z": true\n}')

  const a3 = altT(mktuple(decode({ a: number }, { name: 'AAA' }), decode({ b: string }, { name: 'BBB' })), { name: 'AAAorBBB' })
  throws(() => a3({ z: true }), 'TypeError: Whilist decoding an AAAorBBB got an object but was expecting an AAA or a BBB\nin: {\n  "z": true\n}')

  const a4 = altT(mktuple(path(['a'], number), path(['b'], string)), { onlyOne })
  checkDecoder<number | string, typeof a4>('')

  equals(a4({ a: 1, b: 2 }), 1)
  equals(a4({ a: 'a', b: 'b' }), 'b')
  throws(() => a4({ a: 1, b: 'b' }), 'DecodeError: Was expecting onlyOne of 2 decoders to match whilst decoding an alternate')

})

test('everyT decoder', () => {

  const e1 = every({ n: number }, { s: string })
  checkDecoder<[{ n: number }, { s: string }], typeof e1>('')
  // @ts-expect-error 
  checkDecoder<{ n: number } | { s: string }, typeof e1>('')

  equals(e1({ n: 1, s: 's' }), [{ n: 1 }, { s: 's' }])
  throws(() => e1({ n: 1, s: false }), 'TypeError: Got a boolean (false) but was expecting a string at object.s')


  const e2 = every(path('s', string), path('n', number))
  checkDecoder<[string, number], typeof e2>('')
  // @ts-expect-error
  checkDecoder<(string | number)[], typeof e2>('')
  // @ts-expect-error
  checkDecoder<string | number, typeof e2>('')

  equals(e2({ n: 1, s: 'a' }), ['a', 1])
  throws(() => e2({ n: 1, s: false }), 'TypeError: Got a boolean (false) but was expecting a string at object.s')

  const e3 = everyT(mktuple(path('s', string), path('n', number)), { name: 'SnN' })
  checkDecoder<[string, number], typeof e3>('')
  // @ts-expect-error
  checkDecoder<(string | number)[], typeof e3>('')
  // @ts-expect-error
  checkDecoder<string | number, typeof e3>('')

  equals(e3({ n: 1, s: 'a' }), ['a', 1])
  throws(() => e3({ n: 1, s: false }), 'TypeError: Got a boolean (false) but was expecting a string at SnN.s')

  const a1 = everyO({ a: object({ s: string }), b: path('n', number) })
  checkDecoder<{ a: { s: string }, b: number }, typeof a1>('')

  equals(a1({ n: 2, s: 'b' }), { a: { s: 'b' }, b: 2 })
  throws(() => a1({ n: 2, s: false }), 'TypeError: Got a boolean (false) but was expecting a string at object.s')

})


// -----------------------------------------
//
// select decoder based on: field value / json type
// 

test('lookup Decoders', () => {

  type A = { a: string }
  const decodeA = object({ a: string })
  checkDecoder<{ a: string }, typeof decodeA>('')

  type B = { b: number }
  const decodeB = object({ b: number })
  checkDecoder<{ b: number }, typeof decodeB>('')

  type AB = A | B
  const decodeAB = lookup('t', { a: object({ a: string }), b: object({ b: number }) })
  checkDecoder<AB, typeof decodeAB>('')
  // @ts-expect-error 
  checkDecoder<A & B, typeof decodeAB>('')

  equals(lookup('t', { a: decodeA, b: decodeB })({ t: 'a', a: 'a' }), { a: 'a' })
  equals(decodeAB({ t: 'b', b: 123 }), { b: 123 })

  equals(array(decodeAB)([{ t: 'b', b: 1 }, { t: 'a', a: 'a' }]), [{ b: 1 }, { a: 'a' }])
})

test('type decoder', () => {

  const id = <A>(a: A) => a

  const tp0 = type({
    string: u => String(u + ''),
  })

  checkDecoder<string, typeof tp0>('')

  const tp1 = type({
    string: s => s, // string,
    array: withDecoder([string], a => a.join('/'), { ignoringErrors }), // make sure we have an array of strings.
    number: decoder(u => 'x' + String(u))
  })

  checkDecoder<string, typeof tp1>('')
  // @ts-expect-error 
  checkDecoder<{ string: string, array: string, number: string }, typeof tp1>('')

  equals(tp1('a string'), 'a string')
  equals(tp1(['a', 'b', false, 'c']), 'a/b/c')
  equals(tp1(42), 'x42')

})

test('json decoder', () => {
  const t1 = [{ a: 1, b: { c: true, c2: 'c2' } }, null, [1, '2', null, [true, {}]], true]
  equals(jsonValue(t1), t1)

  throws(() => jsonValue({ d: new Date() }), 'TypeError: Got invalid JSON (class: Date) but was expecting an array, a boolean, a null, a number, an object or a string at dict.d')
  throws(() => jsonValue({ d: undefined }), 'TypeError: Got invalid JSON (undefined) but was expecting an array, a boolean, a null, a number, an object or a string at dict.d')


})

// ----------------------------
//
//  access an element at a path location and then try the given decoder

test('test path', () => {
  equals(path('a', string)({ a: 'a' }), 'a')
  equals(path(0, string)(['b']), 'b')
  equals(path(['c'], string)({ c: 'c' }), 'c')
  equals(path([0], string)(['d']), 'd')
  equals(path([0, 0, 0], string)([[['e']]]), 'e')
  equals(path([0, 'f', 0], string)([{ f: ['f'] }]), 'f')

  equals(path([], string)('c'), 'c')
  equals(path('', string)('c'), 'c')

  equals(object({ b: path([null, 'a'], string) })({ a: 'a' }), { b: 'a' })

  throws(() => path([0, 'f', 0], number)([{ f: ['f'] }]), 'TypeError: Got a string ("f") but was expecting a number at array[0].f[0]')
  throws(() => path([9, 'f', 0], number)([{ f: ['f'] }]), 'TypeError: Got invalid JSON (undefined) but was expecting an object or an array at array[9]')
  throws(() => path([0, 'f', 9], number)([{ f: ['f'] }]), 'TypeError: Got invalid JSON (undefined) but was expecting a number at array[0].f[9]')

  equals(path([0, 'f', 9], opt(number, { ignoringErrors }))([{ f: ['f'] }]), undefined)
  equals(path([0, 'f', 9], def(number, 99))([{ f: ['f'] }]), 99)
})

test('arg', () => {
  const a1 = arg({ a: number, b: string })
  const bad = { a: 1, b: 'b' }

  checkDecoder<number | string, typeof a1>('')
  equals(a1({ a: 1, b: 2 }), 1)
  equals(a1({ a: 'a', b: 'b' }), 'b')

  throws(() => a1(bad), 'DecodeError: Was expecting onlyOne of 2 decoders to match whilst decoding an argument')
})

test('argsT / args', () => {

  const as1 = argsT(mktuple({ a: number }, { b: { c: string, c2: number } }))
  checkDecoder<[number, string | number], typeof as1>('')

  equals(as1({ a: 1, b: { c: 'c', c2: true } }), [1, 'c'])
  equals(as1({ a: 1, b: { c: false, c2: 2 } }), [1, 2])

  throws(() => as1({ a: 1, b: { c: 'c', c2: 2 } }), 'DecodeError: Was expecting onlyOne of 2 decoders to match whilst decoding an object at object.b')

  const as2 = args({ a: number }, { b: { c: string, c2: number } })
  checkDecoder<[number, string | number], typeof as2>('')

  equals(as2({ a: 1, b: { c: 'c', c2: true } }), [1, 'c'])
  equals(as2({ a: 1, b: { c: false, c2: 2 } }), [1, 2])

  throws(() => as2({ a: 1, b: { c: 'cc', c2: 2 } }), 'DecodeError: Was expecting onlyOne of 2 decoders to match whilst decoding an object at object.b')

})

test('callT / invoke', () => {
  const foo = (s: string, n: number) => s.length ** n

  const ad = mktuple({ a: string }, { b: { c: number } })
  const i1 = callT(foo, ad)
  checkDecoder<number, typeof i1>('')

  equals(i1({ a: 'abc', b: { c: 2 } }), 9)
  throws(() => i1({ a: 'abc', b: { c: true } }), 'TypeError: Got a boolean (true) but was expecting a number at object.b.c')

  const i2 = call(foo, { a: string }, { b: number })
  checkDecoder<number, typeof i2>('')
  equals(i2({ a: 'abc', b: 3 }), 27)

})

class Foo {
  constructor(readonly s: string, readonly b: boolean) { }
  public value(): number {
    return this.s.length + (this.b ? 10 : 100)
  }
}

test('callT / invoke', () => {
  const c1 = call(constructor(Foo), { s: string }, { b: boolean })
  checkDecoder<Foo, typeof c1>('')

  equals(c1({ s: 'sss', b: true }).value(), 13)
  throws(() => c1({ s: 'sss', b: 0 }), 'TypeError: Got a number (0) but was expecting a boolean at object.b')

})

// -------------------------------
//
//  Set  / Map / Date
//

test('isodate decoder', () => {

  equals(isodate('2020-01-28T09:07:22.718Z').getTime(), 1580202442718)
  equals(isodate('2020-01-28T09:07:22').getTime(), 1580202442000)

  equals(date('2020-01-28T09:07:22.718Z').getTime(), 1580202442718)
  equals(date('2020-01-28T09:07:22').getTime(), 1580202442000)

  equals(date(1580202442718).getTime(), 1580202442718)

  throws(() => date('2020-01-28').getTime(), 'TypeError: Whilist decoding an isoDate string got a string ("2020-01-28") but was expecting a conforming iso8601 string')
  throws(() => date('09:07:22').getTime(), 'TypeError: Whilist decoding an isoDate string got a string ("09:07:22") but was expecting a conforming iso8601 string')

})

// ---------------------------

test('set decoders', () => {
  const s1 = set(number)
  checkDecoder<Set<number>, typeof s1>('')
  // @ts-expect-error 
  checkDecoder<number[], typeof s1>('')
  // @ts-expect-error 
  checkDecoder<[number], typeof s1>('')
  // @ts-expect-error 
  checkDecoder<Set<number[]>, typeof s1>('')

  equals(s1([1, 2, 3]), new Set([1, 2, 3]))
  equals(set(alt(string, number))(['a', 1]), new Set(['a', 1]))
  throws(() => set(alt(string, number))(['a', 1, true]), 'TypeError: Got a boolean (true) but was expecting a string or a number at array[2]')
  throws(() => set(alt(string, number), { name: 'Fs' })(['a', 1, true]), 'TypeError: Got a boolean (true) but was expecting a string or a number at Fs[2]')
  equals(set(alt(string, number), { ignoringErrors })(['a', 1, true]), new Set(['a', 1]))

})

test('arraySafe Decoders chain exceptions', () => {
  equals(array(u => { if (typeof u === 'number') return u; throw 'BARF' }, { ignoringErrors })([1, '2', 3]), [1, 3])
})


test('map decoder', () => {

  const shortString = validate(string, { "too long": s => s.length < 3 }, { name: 'string-length' })

  const mp = map(shortString, number)
  const mps = map(shortString, number, { ignoringErrors })

  checkDecoder<Map<string, number>, typeof mp>('')

  equals(mp([['a', 1], ['b', 2]]), new Map([['a', 1], ['b', 2]]))
  equals(mp({ a: 1, b: 2 }), new Map([['a', 1], ['b', 2]]))
  equals(mp([{ key: 'a', value: 1 }, { key: 'b', value: 2 }]), new Map([['a', 1], ['b', 2]]))
  equals(mps({ a: 1, b: true }), new Map([['a', 1]]))
  equals(mp({}), new Map())
  equals(mp([]), new Map())

  throws(() => mp(new Map([['a', 0]])), 'TypeError: Got invalid JSON (class: Map) but was expecting an array or an object')
  throws(() => mp([{ key: 'a', value: 1 }, { key: 45, value: 2 }]), 'TypeError: Got a number (45) but was expecting a string at array[1].key')
  throws(() => mp([{ key: 'a', value: 1 }, { key: 'b', value: true }]), 'TypeError: Got a boolean (true) but was expecting a number at array[1].value')
  throws(() => mp({ a: 1, b: true }), 'TypeError: Got a boolean (true) but was expecting a number at object.b')

  throws(() => mp({ a: 1, longString: 2 }), 'DecodeError: string-length validator failed (with: too long) whilst decoding a string ("longString") at object.longString')

  const p = object({
    id: string,
    n: number,
    s: string,
  })
  equals(map(string, p, { keyPath: 'id', valuePath: '' })([{ id: 'id', n: 1, s: 's' }]), new Map([['id', { id: 'id', n: 1, s: 's' }]]))

})

test('numberString decoder', () => {
  equals(numberString('2020'), 2020)
  equals(numberString('NaN'), NaN)
  equals(numberString('Infinity'), Infinity)
  throws(() => numberString('barf'), 'TypeError: Got a string ("barf") but was expecting a numberString')
})

// -------------
// 
// validate

test('validator Decoders', () => {
  const p0 = withDecoder(string, _s => 42)
  checkDecoder<number, typeof p0>('')

  equals(p0('foo'), 42)

  const p1 = validate(string, { isxx: s => s === 'xx' }, { name: 'Foo' })
  checkDecoder<string, typeof p1>('')

  equals(p1('xx'), 'xx')
  throws(() => p1('yy'), 'DecodeError: Foo validator failed (with: isxx) whilst decoding a Foo ("yy")')

  const p2 = withDecoder(string, s => s === <string>'accept' ? s : s.length)
  checkDecoder<number | string, typeof p2>('')

  equals(p2('accept'), 'accept')
  equals(p2('nope'), 4)
  throws(() => p2(4), 'TypeError: Got a number (4) but was expecting a string\nin: 4')

  const v3 = (s: string) => {
    switch (s.length % 4) {
      case 0: return s.length
      case 1: return true
      case 2: return false
      default: return s
    }
  }
  const p3 = withDecoder(string, v3)
  checkDecoder<string | number | boolean, typeof p3>('')

  equals(p3('mod0'), 4)
  equals(p3('mod 1'), true)
  equals(p3('mod  2'), false)
  equals(p3('mod   3'), 'mod   3')
  throws(() => p3(false), 'TypeError: Got a boolean (false) but was expecting a string\nin: false')

})

// --------------------------------
//
// withDecoder

test('withDecoder', () => {
  equals(withDecoder(string, s => s.length)('abc'), 3)
  equals(withDecoder(array(string), a => a.length)(['', '', '']), 3)
})

// --------------------------------
//
// ternary

test('ternary', () => {
  const t = ternary(object({ a: string }), constant('pass'), constant(0))
  equals(t({ a: '' }), 'pass')
  equals(t({ a: false }), 0)

  checkDecoder<string | number, typeof t>('')
  // @ts-expect-error 
  checkDecoder<string & number, typeof t>('')

  throws(
    () => ternary(object({ a: string }), literalValue(['pass']), literalValue(['fail']))({ a: 'a' }),
    'TypeError: Got an object but was expecting one of: ["pass"]'
  )
  throws(
    () => ternary(object({ a: string }), literalValue(['pass']), literalValue(['fail']))({ a: 0 }),
    'TypeError: Got an object but was expecting one of: ["fail"]',
  )
})

// ------------------------
// 
// decoderInfo

test('decoderInfo', () => {
  equals(decoderInfo(string)?.expected, ['string'])
  equals(decoderInfo(tuple(string, boolean))?.expected, ['tuple'])
  equals(decoderInfo(literalobject)?.expected, ['object'])
  equals(decoderInfo(alt(string, boolean))?.expected, ['alternate'])
  equals(decoderInfo(date)?.expected, ['ISO datestring or epoch'])
})

