import { decode, string, set, number, map, tuple, date, checkDecoder, stringLiteral, literalValue, dict, isodate, Dict } from "../src/index"
import { test } from 'mocha'

test('demo 2', () => {

  const mammal = stringLiteral('cat', 'dog', 'cow') // decoders are just functions.
  type Mammal = ReturnType<typeof mammal>           // 'cat' | 'dog' | 'cow'

  const decodeBar = decode({                  // an object
    bar: mammal,                              //   decoders are composable
    ber: literalValue(['one', 'two', 3]),     //   match one of the given values.
    bir: set(mammal),                         //   converts JSON array into a JS Set<Mammal>
    bor: map(number, tuple(string, date)),    //   date decodes epoch or full iso8601 string
    bur: dict(isodate),                       //    { [_key: string]: Date }
  }, { name: 'Bar' })                         // Name the decoder for error messages.

  // Auto derived type of Bar
  type Bar = ReturnType<typeof decodeBar>

  //   type Bar = {
  //     bar: "cat" | "dog" | "cow",
  //     ber: string | number,
  //     bir: Set<"cat" | "dog" | "cow">,
  //     bor: Map<number, [string, Date]>,
  //     bur: Dict<Date>,
  // }

  // Test that the derived type is what you expect
  type ExpectedBar = {
    bar: "cat" | "dog" | "cow",
    ber: string | number,
    bir: Set<"cat" | "dog" | "cow">,
    bor: Map<number, [string, Date]>,
    bur: Dict<Date>,
  }
  checkDecoder<ExpectedBar, typeof decodeBar>('') // will fail to typecheck on a miss match

})