import { validate, decode, decoder, string, number, checkDecoder, alt, boolean, constant, everyO, every, withDecoder } from "../src/index"
import { test } from 'mocha'

class Person { constructor(readonly name: string) { } }

test('demo 3', () => {

  const decodePap = decode({
    pap: withDecoder([string], a => new Person(a.join(','))), // decode an array of strings, then transform.
    pep: decoder((u: unknown): string => {                    // wrap a user function into a combinator,
      if (typeof (u) != 'boolean') { throw new Error('not a boolean') }  //   handling errors as needed.
      return u ? 'success' : 'error'
    }),
    pip: validate(string, {                                   // use the decoder, then validate 
      lengthGE3: s => s.length >= 3,                          //   against named validators.
      lengthLE10: s => s.length <= 10,                        //   All validators have to be true.
    }),
  })

  type Pap = ReturnType<typeof decodePap>
  // type Pap = {
  //   pap: Person;
  //   pep: string;
  //   pip: string;
  // }

  type ExpectedPap = {
    pap: Person,
    pep: string,
    pip: string,
  }

  checkDecoder<ExpectedPap, typeof decodePap>('')

  try {
    decodePap(JSON.parse(
      '{"pap": ["one",2], "pep":"true","pip": "12345678901234" }'
    ))
  } catch (e) { 
    // console.log(e.message)
    // TypeError: Got a number (2) but was expecting a string at object.pap[1]
    // DecodeError: UserDecoder threw: 'not a boolean' whilst decoding a string ("true") at object.pep
    // DecodeError: validation failed (with: lengthLE10) whilst decoding a string ("12345678901234") at object.pip
    // in: {
    //   "pap": [
    //     "one",
    //     2
    //   ],
    //   "pep": "true",
    //   "pip": "12345678901234"
    // }
  }

})