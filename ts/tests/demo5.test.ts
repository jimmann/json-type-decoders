import { JsonValue, decoder, string, number, checkDecoder, jsonValue, constant, lookup, type, path, ternary, withDecoder } from "../src/index"
import { test } from 'mocha'

test('demo 5', () => {

  const decodeBSR = lookup('type', {                    // decode an object, get field 'type' & lookup the decoder to use
    body: {                                             // if the 'type' field === 'body' use the following decoder:
      body: jsonValue,                                  //  deep copy of source JSON ensuring no non-Json constructs (eg Classes)
      typeOfA: path('^.json.a', decoder(j => typeof j)) //  try a decoder at a different path in the source JSON
    },
    status: ternary(                                    // if the 'type' field === 'status'
      { ver: 1 },                                       //  test that there is a 'ver' field with the value 1
      { status: withDecoder(number, n => String(n)) },  //    'ver' === 1 : convert 'status' to a string.
      { status: string },                               //    otherwise   : decode a string
    ),
    result: {                                           // if the 'type' field === 'result'
      result: type({                                    //  decode the result field based on its type
        number: n => n + 100,                           //    in all cases return a number
        boolean: b => b ? 1 : 0,
        string: s => Number(s),
        array: a => a.length,
        object: o => Object.keys(o).length,
        null: constant(-1)                              //    ignore the provided value (null) and return -1
      })
    }
  })

  type ActualBSR = ReturnType<typeof decodeBSR>

  type ExpectedBSR = {                                  // Note that the 'type' is NOT in the derived type.
    body: JsonValue;
    typeOfA: "string" | "number" | "bigint" | "boolean" | "symbol" | "undefined" | "object" | "function"
  } | {
    status: string
  } | {
    result: number
  }

  checkDecoder<ExpectedBSR, typeof decodeBSR>('')

  const res = decodeBSR({ type: 'result', result: [200] })
  // console.log('res =', res);
  // res = { result: 1 }

})