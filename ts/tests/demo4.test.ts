import { opt, array, number, tuple, ignoringErrors, checkDecoder, alt, type, withDecoder, validate } from "../src/index"
import { test } from 'mocha'

test('demo 4', () => {

  const decodeDate = validate(type({
    number: n => new Date(n * 1000),                // typeof n is a number
    string: s => {
      const parts = s.match(/(\d{4})-(\d{2})-(\d{2})[T ](\d{2}):(\d{2}):(\d{2})\.?(\d{0,3})/)
      return parts ? new Date(Date.UTC(
        +parts[1], +parts[2] - 1, +parts[3], +parts[4], +parts[5], +parts[6],
        +((parts[7] ?? '0') + '00').substring(0, 3)
      )) : new Date(NaN)                            // invalid date will get caught by validate()
    },
    array: withDecoder(                             // decode a tuple suitable for passing to Date()
      tuple(number, number, opt(number), opt(number), opt(number), opt(number), opt(number)),
      ns => new Date(Date.UTC(...ns))
    ),
    object: withDecoder(
      alt(
        { Y: number, m: number, d: number, H: number, M: number, S: number, N: number },
        { seconds: number, milliSeconds: number }),
      o => 'Y' in o
        ? new Date(Date.UTC(o.Y, o.m, o.d, o.H, o.M, o.S, o.N / 1e6))
        : new Date(o.seconds * 1000 + o.milliSeconds)
    )
  }), {
    invalidDate: d => !isNaN(d.getTime()),
  })

  checkDecoder<Date, typeof decodeDate>('')


  const res = array(opt(decodeDate, { ignoringErrors }))([
    '2021-02-21T09:16:34.123-10:00',
    1613898994.123,
    [2021, 1, 21, 9, 16, 34, 123],
    { Y: 2021, m: 1, d: 21, H: 9, M: 16, S: 34, N: 123000000 },
    { seconds: 1613898994, milliSeconds: 123 }
  ])

  // console.log('res', res)
  // res [
  //   2021-02-21T09:16:34.123Z,
  //   2021-02-21T09:16:34.123Z,
  //   2021-02-21T09:16:34.123Z,
  //   2021-02-21T09:16:34.123Z,
  //   2021-02-21T09:16:34.123Z
  // ]

})