import { construct, everyT, decode, decoder, string, args, JsonValue, object, opt, array, set, number, map, tuple, date, ignoringErrors, checkDecoder, alt, altT, boolean, stringLiteral, literalValue, jsonValue, def, constant, dict, isodate, Dict, everyO, lookup, type, path, UP, ternary, every, withDecoder, validate, call, constructor } from "../src/index"
import { test } from 'mocha'


function fn(
  petCount: number,
  petType: string,
  extras: ('cat' | 'dog')[],
): string {
  return `${petCount} ${petType}s and [${extras.join(', ')}]`
}

class Zoo {

  constructor(
    private petType: string,
    private petCount: number,
    private extras: ('cat' | 'dog')[],
  ) { }

  public get salesPitch() { return `${this.petCount} ${this.petType}s and [${this.extras.join(', ')}]` }

  // static method to decode the class
  static decode = construct(                // does the 'new' stuff ...
    Zoo,                                    // Class to construct
    path('somewhere.deeply.nested', string),// 1st arg, a string at the given path
    { petCount: number },                   // 2nd arg, a number from the location
    { pets: array(stringLiteral('cat', 'dog')) }, // 3rd arg, an array of cats/dogs
  )
}

test('demo 6', () => {

  const argsDecoder = args(                           // decode a tuple (suitable for using as function args) by
    { petCount: number },                             // following a 'path' to a value, and then trying a decoder
    { somewhere: { deeply: { nested: string } } },
    { pets: array(stringLiteral('cat', 'dog')) },     // Note: have to use array() here
  )

  checkDecoder<[number, string, ("cat" | "dog")[]], typeof argsDecoder>('')

  // Alternatively using path

  const argsDecoder2 = tuple(
    path('petCount', number),
    path('somewhere.deeply.nested', string),
    path('pets', [stringLiteral('cat', 'dog')])
  )

  checkDecoder<[number, string, ("cat" | "dog")[]], typeof argsDecoder2>('')

  const json = JSON.parse('{"pets":["cat","cat","dog"],"petCount":42,"somewhere":{"deeply":{"nested":"cow"}}}')

  const fnArgs = argsDecoder(json)

  const res = fn(...fnArgs)   // this will fail to typecheck if the argument types are wrong

  // console.log('res = ' + res)
  // res= 42 cows and [cat, cat, dog]

  // All together:

  const getPetString = call(                        // call a user function with the decoded arguments
    fn,                                             // the type of the function args HAS to match the 
    { petCount: number },                           // subsequent argument decoders
    { somewhere: { deeply: { nested: string } } },
    { pets: array(stringLiteral('cat', 'dog')) },
  )

  // console.log('getPetString:',getPetString(json))
  // getPetString: 42 cows and [cat, cat, dog]

  // console.log('Zoo.decode().pitch:', Zoo.decode(json).salesPitch)
  //  getZoo.pitch: 42 cows and [cat, cat, dog]

  // With invalid input:
  // console.log('Zoo.decode().pitch:', Zoo.decode({ ...json, somewhere: null }).salesPitch)
  //  Error: TypeError: Got a null but was expecting an object or an array at Zoo.somewhere
  //  in: {
  //    "pets": [
  //      "cat",
  //      "cat",
  //      "dog"
  //    ],
  //    "petCount": 42,
  //    "somewhere": null
  //  }
})