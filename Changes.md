# Changelog

### 0.4.0
  - fixed loose try/catch handling to fix TS errors.

### 0.3.0

  - Typescript 4.2.4
  - Added `objectLike` option for the `object` decoder (for `process.env` decoding)
  - Added `numberString` decoder
  - Added `literalobjectLike` decoder
  - Fixed the `throws` test function(!)
  - Fixed the (internal) `TupleMapUnDecoder<>` generic.
  - Moved `./dist` to `./build`

### 0.2.2

  - started Changelog.